<?php
class mod_my_catalogo extends myModulo{
	function __construct($nombre){	    ;
		$doc = myApp::getDocumento();
		$doc->incluirJQuery();
		parent::__construct($nombre);
	}  
    
    function mostrarIndex(){
        require_once(JPATH_ROOT.DS."myCore".DS."myApp.php");
        $com = myComponente::crearComponente("com_my_catalogo", "carrito");
        $this->tmplVars["contenido"] = $com->listaNovedades(false);        
        $this->render("index.twig");
    }
}
?>