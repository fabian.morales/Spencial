<?php
class mod_my_carrito extends myModulo{
	function __construct($nombre){	    
		$doc = myApp::getDocumento();
		$doc->incluirJQuery();		
		parent::__construct($nombre);
        $doc->addEstilo(JUri::root()."components/com_my_catalogo/css/com_my_catalogo.css");
	}
	
    function getIdSession(){
        $sesion =& JFactory::getSession();
        $idSesion = $sesion->get("id_sesion");        
        if (!sizeof($idSesion)){
            $idSesion = uniqid();
            $sesion->set("id_sesion", $idSesion);
        }
        
        return $idSesion;
    }
	    
	function mostrarIndex(){
        $info = $this->modelo->obtenerTotales($this->getIdSession());
		if (!sizeof($info)){
			$info = array("cantidad" => 0, "valor" => 0);
		}
		$this->tmplVars["info"] = $info;
		$this->render("index");
	}
}
?>