(function(window, $){
	var my_catalogo = {	

	};
	
	function agregarItemCarrito(e){
		e.preventDefault();
        var dataRef = $(this).attr("data-ref");
                
        var dataColor = $("input.color[data-ref="+ dataRef +"]:checked");
		var dataCantidad = $("input.inputCantidad[data-ref="+ dataRef +"]");
		var dataTalla = $("select.selectTalla[data-ref="+ dataRef +"]");
				
		var cantidad = dataCantidad.val();
		var color = dataColor.val();
		var talla = dataTalla.val();
		
		if (dataCantidad != null && cantidad <= 0){
			alert('Debe seleccionar la cantidad');
			return;
		}
		
		if (dataColor != null && color == 0){
			alert('Debe seleccionar el color');
			return;			
		}

		if (dataTalla != null && talla == 0){
			alert('Debe seleccionar la talla');
			return;			
		}
				
		$.ajax({
			url: $(this).attr("href"),
			data: {idColor: color, idTalla: talla, cantidad: cantidad, suma: 'Y', format : 'raw'},
			success: function(res){
				$("#divModInfoCarrito").html(res);
                //window.location.href = 'index.php?option=com_my_catalogo&helper=carrito';
			}
		});
	}
	
    function guardarDetalleItemCarrito(e){
        e.preventDefault();
        var inputCantidad = "#" + $(this).attr("data-cantidad");
        var cantidad = $(inputCantidad).val();
        
        if (cantidad <= 0){
            alert('Debe ingresar la cantidad');
            return;
        }
        
        $.ajax({
            url: $(this).attr("href"),
            data: {cantidad: cantidad, template: 'mostrarDetalle', format : 'raw'},
            success: function(res){
                $("#divItemscarrito").html(res);
                mostrarModulo();
                hookEvents();
            }
        });
    }
    
    function borrarItemCarrito(e){
        e.preventDefault();
        
        $.ajax({
            url: $(this).attr("href"),
            data: {format : 'raw'},
            success: function(res){
                $("#divItemscarrito").html(res);
                mostrarModulo();
                hookEvents();
            }
        });
    }
    
    function mostrarModulo(){
         $.ajax({
            url: "index.php?option=com_my_catalogo&helper=carrito&task=mostrarModuloCarrito&format=raw",            
            success: function(res){
                $("#divModInfoCarrito").html(res);                
            }
        });
    }
    	
	function asignarImagenGal(objImg){
		
		var id = "#" + objImg.attr("data-imagen");		
		setTimeout(function() {
			$(".imgGalArticuloActivo").fadeTo(200, 0).removeClass("imgGalArticuloActivo");
		}, 10);
		
		setTimeout(function() {
			$(id).addClass("imgGalArticuloActivo").fadeTo(200, 1);	
		}, 10);
		
		$(".itemThumbGalActivo").removeClass("itemThumbGalActivo");
		objImg.addClass("itemThumbGalActivo");
        
        /*$("a[rel='imgGaleria']").jqzoom({  
                  zoomType: 'standart',  
                  lens:true,  
                  preloadImages: false,  
                  alwaysOn:false,  
                  zoomWidth: 300,  
                  zoomHeight: 250,  
                  xOffset:200,  
                  yOffset:0,  
                  position:'left' 
         });*/        
	}
	
	function hookFormUsuarios(){        
		$("a[rel='login']").unbind("click");
		$("a[rel='login']").bind("click", function(e) {
		    e.preventDefault();
	    	formLogin.submit();
		});
		
		$("a[rel='crear_usuario']").unbind("click");
        $("a[rel='crear_usuario']").bind("click", 	function (e){
	    	e.preventDefault();
	    	formCrearUsuario.submit();
		});

	}
	
	function hookEvents(){
		$("div.color").unbind("click");
		$("div.color").bind("click", function() {
			$(this).parent().find(".colorActivo").removeClass("colorActivo");
			$(this).addClass("colorActivo");
			var inputColor = "#" + $(this).attr("data-color");
			var idColor = $(this).attr("data-idColor");
			$(inputColor).val(idColor);
		});
		
		$("img.imgThumbGal").unbind("click");
		$("img.imgThumbGal").bind("click", function(e) {
			e.preventDefault();			
			asignarImagenGal($(this));
		});
		
		$("a[rel='agrCarrito']").unbind("click");
        $("a[rel='agrCarrito']").bind("click", agregarItemCarrito);

		$("a[rel='actualizarItemCarrito']").unbind("click");
        $("a[rel='actualizarItemCarrito']").bind("click", guardarDetalleItemCarrito);
        
		$("a[rel='borrarItemCarrito']").unbind("click");
        $("a[rel='borrarItemCarrito']").bind("click", borrarItemCarrito);
        
        hookFormUsuarios();      
		$("a[rel='linkLogin']").fancybox({width:"80%", height:"80%", type:'ajax', afterShow: hookFormUsuarios});
        
		$("a[rel='formDireccion']").fancybox({width:"80%", height:"80%", type:'ajax', afterShow: function() {  
				$("a[rel='guardarDireccion']").unbind("click");
				$("a[rel='guardarDireccion']").bind("click", function (e){
					e.preventDefault();        
					formDireccion.submit();
				});
			} 
		});
		    	
    	$("#btnGuardarUsuario").unbind("click");
    	$("#btnGuardarUsuario").bind("click", function (e) {
    		e.preventDefault();
    		formCrearUsuario.submit();
    	});
	}
	
	function inicioDoc(){
		$("a[rel='imgGaleria']").fancybox();		
		asignarImagenGal($(".imgThumbGal").first());
		/*$(".detalleArticulo").css("width", "100%").css("width", "-=250px");
		$(".listaReferenciasConj").css("width", "100%").css("width", "-=250px")
		$(".listaReferenciasConj .detalleArticulo").css("width", "100%")*/
		hookEvents();
		
		var envioPago = $("#enviarPago");
		if (envioPago != null){
			if (envioPago.val() == "Y"){
				formPago.submit();
			}
		}
        
        /*$("a[rel='imgGaleria']").easyZoom({
            preload: '<p class="preloader">Cargando...</p>'});*/
	}
		
	window.my_catalogo = my_catalogo;	
	$(document).ready(inicioDoc);
})(window, jQuery);