<?php
class com_my_catalogo extends myComponente{
    var $pedido = null;
    
    public function procesarTarea($tarea = ""){
        $cfg = new myConfig();
        $this->modelo->limpiarCarrito($cfg->vigenciaCarrito);

        if (!sizeof($tarea)){
            $tarea = $this->request->getVar("task");
        }
        
        $this->pedido = $this->obtenerPedidoActual();
        
        $tareas = array("datosEnvio", "mostrarResumenPedido", "realizarPago", "mostrarFormDireccion", "guardarDireccion");
        if (in_array($tarea, $tareas)){
            $user =& JFactory::getUser();
            if (!$user->id){
                myApp::mostrarMensaje("Debe iniciar sesión para poder continuar", "error", true, "mensajes");
                $this->mostrarIndex();
                return;
            }
			
			if (trim($user->activation)){
                myApp::mostrarMensaje("Su usuario aún no ha sido activado.", "alert", true, "mensajes");
                return;				
			}

            $myUser = $this->modelo->getUser($user->id);
            if (!sizeof($myUser)){
                myApp::mostrarMensaje("Sus datos de registro no están completos.\n En su perfil de usuario puede ingresar los datos necesarios.", "alert", true, "mensajes");
                return;
            }                        
            
            if ($this->pedido["valor_items"] == 0 || $this->pedido["num_items"] == 0 || !sizeof($this->pedido["detalle"])){                
                myApp::mostrarMensaje("Su carrito de compra se encuentra vacio", "alert", true, "mensajes");
                return;
            }
            
            $fecha = strtotime(date('Y-m-d H:i:s'));
            $fechaPedido = strtotime($this->pedido["fecha"]);
            $dif = ($fecha - $fechaPedido) / 3600; //tiempo en horas
                   
            if ($this->pedido["estado"] == "P" && $dif < 24){
                myApp::mostrarMensaje("Ya tiene un pedido pendiente esperando respuesta de la plataforma de pagos. Intentelo nuevamente cuando se haya recibido dicha respuesta.", "alert", true, "mensajes");
                return;
            }
        }

		$_msj = $this->request->getVar("_msj");
		$_tipo = $this->request->getVar("_tipoMsj", "mensaje");
		if ($_msj){
			myApp::mostrarMensaje($_msj, $_tipo, true, "mensajes");
		}
                
        return parent::procesarTarea($tarea);
    }

	function __construct(){
		$doc = myApp::getDocumento();
		$doc->incluirJQuery();
		$doc->incluirLibJs("fancybox", array("fancybox"));
        $doc->addEstilo(JUri::root()."myCore/css/gumby.css");
		parent::__construct();
        $idSesion = $this->getIdSession();
        $this->tmplVars["idSesion"] = $idSesion;
        $cfg = new myConfig();
        $this->tmplVars["componenteUsuario"] = $cfg->componenteUsuarios;
	}

    function getIdSession(){
        $sesion =& JFactory::getSession();
        $idSesion = $sesion->get("id_sesion");
        if (!sizeof($idSesion)){
            $idSesion = uniqid();
            $sesion->set("id_sesion", $idSesion);
        }

        return $idSesion;
    }

	function mostrarIndex(){	    
        $this->tmplVars["itemsCarrito"] = $this->itemsCarrito(false);

        $urlEnvio = "index.php?option=com_my_catalogo&helper=carrito&task=datosEnvio";
        $user = &JFactory::getUser();
        if (!$user->id){
            $url = "index.php?option=com_my_catalogo&helper=usuarios&redirect=".base64_encode($urlEnvio)."&format=raw";
            $this->tmplVars["relCompra"] = "rel=\"linkLogin\"";
        }
        else{
            $url = $urlEnvio;
        }

        $this->tmplVars["urlCompra"] = $url;

		$this->render("index");		
	}

    function itemsCarrito($imprimir=true){        
		$this->tmplVars["imgBase"] = JUri::root()."myImagenes/referencias";		
        $this->tmplVars["pedido"] = $this->pedido;
        
        if ($imprimir){
            $this->render("itemsCarrito");
        }
        else{
            return $this->renderStr("itemsCarrito");
        }
    }

    function guardarItemCarrito(){
        $idSesion = $this->getIdSession();        
        $idReferencia = $this->request->getVar("idReferencia", 0, "int");
        $idColor = $this->request->getVar("idColor", 0, "int");
        $idTalla = $this->request->getVar("idTalla", 0, "int");
        $ext = $this->modelo->getExtension($idReferencia, $idColor, $idTalla);        
        
        $cantidad = $this->request->getVar("cantidad", 1, "int");
        $suma = $this->request->getVar("suma");
        $template = $this->request->getVar("tpl");
        $idUsuario = "0";
		$idRegistro = uniqid();

		$item = $this->modelo->getItem("", $idSesion, $idReferencia, $ext["id"]);
        if (sizeof($item)){            	
        	$idRegistro = $item["id_reg"];
        	if ($suma == "Y"){
            	$cantidad += $item["cantidad"];
        	}
        }
		
        if ($this->modelo->guardarItem($idRegistro, $idUsuario, $idSesion, $idReferencia, $ext["id"], $cantidad)){
            if ($template == "mostrarDetalle"){
                myApp::redirect("index.php?option=com_my_catalogo&helper=carrito");
            }
            else{
                $this->mostrarModuloCarrito();
            }            
        }
    }

    function borrarItemCarrito(){
        $sesion =& JFactory::getSession();
        $sesion->clear("comprando");

        $idSesion = $this->getIdSession();
        $idReferencia = $this->request->getVar("idReferencia", 0, "int");        
        $idExt = $this->request->getVar("idExt", 0, "int");
		
		$item = $this->modelo->getItem("", $idSesion, $idReferencia, $idExt);
        if (sizeof($item)){            	        	
			$this->modelo->borrarItem($item["id_reg"]);
        }
		        
        $this->itemsCarrito();
    }

	function mostrarModuloCarrito(){
        $info = $this->modelo->obtenerTotales($this->getIdSession());
		if (!sizeof($info)){
			$info = array("cantidad" => 0, "valor" => 0);
		}

        $this->tmplVars["info"] = $info;
		$this->render("moduloCarrito");
	}

	function datosEnvio(){
		$user =& JFactory::getUser();
		$myUser = $this->modelo->getUser($user->id);
		$this->tmplVars["usuario"] = $myUser;
				
        $this->tmplVars["direccion"] = $myUser["direccion"]." - ".$myUser["ciudad"]." (".$myUser["depto"].", ".$myUser["nombre_pais"].")";
		
		$this->render("datosEnvio");
	}

	function mostrarFormDireccion(){
	    $user =& JFactory::getUser();
        $this->tmplVars["usuario"] = $this->modelo->getUser($user->id);
		$this->tmplVars["paises"] = $this->modelo->obtenerListaPaises();
        $this->render("formDireccion");
	}

    function guardarDireccion(){        
        $idUser = $this->request->getVar("id_user");
        $direccion = $this->request->getVar("direccion");
        $idPais = $this->request->getVar("idPais");
        $ciudad = $this->request->getVar("ciudad");
        $depto = $this->request->getVar("depto");
		
        $mensaje = "";
        $tipo = "";
		
        if (!$ciudad){
            myApp::mostrarMensaje("Debe ingresar la ciudad", "error", true, "mensajes");
            return false;
        }
		
        if (!$depto){
            myApp::mostrarMensaje("Debe ingresar el departamento o provincia", "error", true, "mensajes");
            return false;
        }

        if ($this->modelo->guardarDireccion($idUser, $direccion, $idPais, $ciudad, $depto)){
            $mensaje = "Dirección guardada exitosamente";
            $tipo = "mensaje";
        }
        else{
            $mensaje = "No se pudo guardar la dirección";
            $tipo = "error";
        }

        myApp::redirect("index.php?option=".$this->nombreComp."&helper=carrito&task=datosEnvio&_msj=".$mensaje."&_tipoMsj=".$tipo);
    }

    function mostrarResumenPedido(){
        $this->tmplVars["pedido"] = $this->pedido;
        $this->render("resumenPedido");        
    }
	
	function lal(){
		$sesion =& JFactory::getSession();
		$this->tmplVars["urlImagenes"] = JUri::root()."components/com_my_catalogo/images/";
		$this->tmplVars["mensajeCorreo"] = base64_decode($sesion->get("resumenPedido"));
		$this->render("mensajeCorreo");
	}

    function realizarPago(){
        $modelo = $this->modelo;

        if ($modelo->guardarPedido(0, $this->pedido["id_user"], $this->pedido["direccion"], $this->pedido["num_items"], $this->pedido["cargo_envio"], $this->pedido["valor_items"], $this->pedido["valor_total"], $this->pedido["porc_iva"], $this->pedido["valor_iva"], $this->pedido["fecha"], $this->pedido["estado"])){
            $id = $modelo->insertId();
            $this->pedido["id"] = $id;            

            foreach ($this->pedido["detalle"] as $p){
                $modelo->guardarDetallePedido($id, $p["id_referencia"], $p["id_ext"], $p["cantidad"], $p["valor"], $p["porc_iva"], $p["valor_iva"]);
            }
        }

        $modelo->vaciarCarrito($this->getIdSession());

        $cfg = new myConfig();
		$_plataforma = $cfg->plataformaPagos;		

		if ($_plataforma){
            $modelo->cambiarEstadoPedido($this->pedido["id"], "P");
	        $plataforma = myPlataformaPago::cargarPlataforma($_plataforma);
	        $plataforma->realizarPago($this->pedido);
		}
		else{
			$this->tmplVars["urlImagenes"] = JUri::root()."components/com_my_catalogo/images/";
			$this->tmplVars["pedido"] = $this->pedido;
			$mensaje = $this->renderStr("mensajeCorreo");
		    
			$user = &JFactory::getUser();
			$jcfg = new JConfig();
			$mail =& JFactory::getMailer();
			$mail->addRecipient($user->email);
			$mail->setSender(array($jcfg->mailfrom, $jcfg->fromname));
			$mail->setSubject("Pedido recibido");
			$mail->IsHTML(1);
			$mail->setBody($mensaje);
			$mail->Send();
			$modelo->cambiarEstadoPedido($this->pedido["id"], "P");
			$this->render("mensajePedidoGuardado");
			//myApp::mostrarMensaje("Su pedido ha sido guardado en nuestra base de datos", "mensaje");
		}

    }

    function respuestaPago(){
        $cfg = new myConfig();
        $plataforma = myPlataformaPago::cargarPlataforma($cfg->plataformaPagos);
        $ret = $plataforma->respuestaPago($this->request->get("GET"));        
        //$this->procesarRespuestaPagos($ret);
        myApp::redirect("index.php", $ret["mensaje"]);
    }

    function confirmacionPago(){
        $cfg = new myConfig();
        $plataforma = myPlataformaPago::cargarPlataforma($cfg->plataformaPagos);
        $ret = $plataforma->confirmacionPago($this->request->get("POST"));
        $this->procesarRespuestaPagos($ret);        
    }
    
    function procesarRespuestaPagos($ret){
        $modelo = $this->modelo;

        if ($ret["error"]){
            //myApp::mostrarMensaje($ret["error"], "error", true, "mensajes");
            myApp::redirect("index.php", $ret["error"]);
        }
        else{
            $pedido = $modelo->getPedido($ret["id_pedido"]);
            $pedido["detalle"] = $modelo->getListaDetallePedido($ret["id_pedido"]);
            if (sizeof($pedido)){
                $tituloMensaje = "";
                $mensajeEstado = "";
                
                switch ($ret["respuesta"]){
                    case "A":{
                        $modelo->cambiarEstadoPedido($pedido["id"], "A");
                        $modelo->asignarTransPedido($pedido["id"], $ret["trans"]);
                        $tituloMensaje = "Transaccion exitosa";
                        $mensajeEstado = "Hemos recibido su pedido correctamente, en breve nos pondremos en contacto con usted";
                        break;
                    }
                    case "P":{
                        $modelo->cambiarEstadoPedido($pedido["id"], "P");
                        $tituloMensaje = "Transaccion pendiente por aprobación";
                        $mensajeEstado = "Su transacción se encuentra pendiente por aprobación de parte de la entidad de pagos en linea";
                        break;
                    }
                    case "R":
                    case "C":{
                        $modelo->cambiarEstadoPedido($pedido["id"], "R");
                        $tituloMensaje = "Transaccion rechazada";
                        $mensajeEstado = "Su transacción ha sido rechazada por la entidad de pagos en linea";
                        break;
                    }
                }
                
                $this->tmplVars["titulo"] = $tituloMensaje;
                $this->tmplVars["mensajeEstado"] = $mensajeEstado;
                $this->tmplVars["pedido"] = $pedido;
                $this->tmplVars["urlImagenes"] = JUri::root()."components/com_my_pago/images/";                
                $mensaje = $this->renderStr("mensajeCorreo.html");

                $jcfg = new JConfig();
                $mail =& JFactory::getMailer();
                $mail->addRecipient($pedido["correo"]);
                $mail->addRecipient($jcfg->correoAdmin);
                $mail->setSender(array($jcfg->mailfrom, $jcfg->fromname));
                $mail->setSubject("Pedido recibido");
                $mail->IsHTML(1);
                $mail->setBody($mensaje);
                $mail->Send();			
            
            }
            else{
                myApp::mostrarMensaje("Pedido no encontrado", "error", true, "mensajes");
            }

			myApp::redirect("index.php", $ret["mensaje"]);
        }
    }

	function obtenerPedidoActual(){
		$modelo = $this->modelo;
		$user =& JFactory::getUser();
		$cfg = new myConfig();
        
        $retPedido = null;
        
        if ($user->id){
            $myUser = $modelo->getUser($user->id);            
            $pedido = $modelo->getListaPedidos($user->id, "", "", 0, 0, true, true);

            if (sizeof($pedido)){
                if ($pedido[0]["estado"] == "N" || $pedido[0]["estado"] == "P"){
                    $retPedido = $pedido[0];
                    $retPedido["detalle"] = $this->modelo->getListaDetallePedido($retPedido["id"]);
                }
            }
        }
                
        if (!sizeof($retPedido)){
            $idSesion = $this->getIdSession();
            $info = $modelo->obtenerTotales($this->getIdSession());
            $retPedido["cargo_envio"] = $cfg->gastosEnvio;
            $retPedido["valor_total"] = $info["valor"] + $cfg->gastosEnvio;
            $retPedido["valor_items"] = $info["valor"];
            $retPedido["num_items"] = $info["cantidad"];
            $retPedido["porc_iva"] = $cfg->porcIva;
            $retPedido["valor_iva"] = $retPedido["valor_total"] - ($retPedido["valor_total"] / (1 + $cfg->porcIva / 100));             
            $retPedido["fecha"] = date('Y-m-d H:i:s');
            $retPedido["estado"] = "N";
                        
			$detalle = array();
			$items = $modelo->getListaItems(0, $idSesion);
			foreach ($items as $i){
				$i["valor"] = $i["valor_base"] * $i["cantidad"];
				$i["porc_iva"] = $cfg->porcIva;
				$i["valor_iva"] = $i["valor"] - ($i["valor"] / (1 + $cfg->porcIva / 100));
				$i["path"] = JUri::root()."myImagenes/referencias/".$i["id_referencia"]."/".$i["id_color"];
				$detalle[] = $i;
			}
            
            $retPedido["detalle"] = $detalle;
        }

        $retPedido["valor_base"] = $retPedido["valor_total"] - $retPedido["valor_iva"];
        $retPedido["id_user"] = $user->id;
        $retPedido["nombre"] = $myUser["nombre"]." ".$myUser["apellido"];		
        $retPedido["direccion"] = $myUser["direccion"]." - ".$myUser["ciudad"]." (".$myUser["depto"].", ".$myUser["nombre_pais"].")";
        $retPedido["telefono"] = $myUser["telefono"];
        
        return $retPedido;
	}
}

?>