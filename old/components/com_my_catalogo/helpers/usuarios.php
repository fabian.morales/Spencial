<?php
class com_my_catalogo extends myComponente{
	// public function procesarTarea($tarea = ""){
		// if (!sizeof($tarea))
			// $tarea = JRequest::getVar("task");

		// $metodo = JRequest::getString("metodo", "");

		// switch ($tarea){
			// case "listaColorRef":{
				// return
			// }
			// default: {
				// return parent::procesarTarea($tarea);
			// }
		// }
	// }

	function __construct(){
	    $cfg = new myConfig();
		$doc = myApp::getDocumento();
		$doc->incluirJQuery();
		$doc->incluirLibJs("fancybox", array("fancybox"));
		parent::__construct();
	}

	function mostrarIndex(){	    
        $this->tmplVars["redirect"] = $this->request->getVar("redirect", "", "base64");
		$this->tmplVars["paises"] = $this->modelo->obtenerListaPaises();
		$this->render("index");		
	}

    function login($username="", $password="", $redirect=""){   
		if (!$redirect){
	        if ($redirect = $this->request->getVar("redirect", "", "base64")) {
	            $redirect = base64_decode($redirect);
	            if (!JURI::isInternal($redirect)) {
	                $redirect = '';
	            }
	        }
		}

        if (!$username){
        	$username = $this->request->getVar('username_login', '', 'username');
        }

		if (!$password){
			$password = $this->request->getVar('password_login', '', "RAW");
		}

        $user = $this->modelo->getUserJoomla(0, $username);

        $opciones = array();
        $opciones['remember'] = false;
        $opciones['return'] = $redirect;

        $credenciales = array();
        $credenciales['username'] = $user["username"];
        $credenciales['password'] = $password;		
        $error = myApp::login($credenciales, $opciones);
				
        if(!JError::isError($error)){
        //if($error === true){
            if (!$redirect) {
                $redirect = 'index.php?option=com_my_catalogo&helper=usuarios';
            }
            
            myApp::redirect($redirect);
        }
        else{        	
			myApp::mostrarMensaje($error->message, "error", true, "mensajes");
            //$this->mostrarIndex();
        }
    }

	function logout(){
		$error = myApp::logout();

        if(!JError::isError($error)){
            if ($redirect = $this->request->getVar('redirect', '', 'base64')){
                $redirect = base64_decode($redirect);
                if (!JURI::isInternal($redirect)) {
                    $redirect = '';
                }
            }

            if ($redirect && !(strpos($redirect, 'com_my_catalogo'))){
                myApp::redirect($redirect);
            }
        }
		else{
            $this->mostrarIndex();
        }
	}

    function guardarUsuario(){
        jimport('joomla.user.helper');
        $modelo = $this->modelo;
        $request = $this->request;

        $email = $request->getVar("email");
        $nombre = $request->getVar("nombre");
        $cedula = $request->getVar("cedula");
        $username = $request->getVar("username");
        $apellido = $request->getVar("apellido");
        $direccion = $request->getVar("direccion");
        $idPais = $request->getVar("idPais");
        $ciudad = $request->getVar("ciudad");
        $depto = $request->getVar("depto");
        $telefono = $request->getVar("telefono");
        $celular = $request->getVar("celular");
        $password = $request->getVar('password', '', JREQUEST_ALLOWRAW);
        $grupo = $this->modelo->getGrupoUser();

		if ($redirect = $request->getVar('_redirect', '', 'base64')) {
            $redirect = base64_decode($redirect);
            if (!JURI::isInternal($redirect)) {
                $redirect = '';
            }
        }

        if (!$email){
            myApp::mostrarMensaje("Debe ingresar el email", "error", true, "mensajes");
            return false;
        }

        if (!$cedula){
            myApp::mostrarMensaje("Debe ingresar la cedula", "error", true, "mensajes");
            return false;
        }

        if (!$nombre){
            myApp::mostrarMensaje("Debe ingresar el nombre", "error", true, "mensajes");
            return false;
        }

        if (!$apellido){
            myApp::mostrarMensaje("Debe ingresar el apellido", "error", true, "mensajes");
            return false;
        }

        if (!$direccion){
            myApp::mostrarMensaje("Debe ingresar la dirección", "error", true, "mensajes");
            return false;
        }
		
        if (!$ciudad){
            myApp::mostrarMensaje("Debe ingresar la ciudad", "error", true, "mensajes");
            return false;
        }
		
        if (!$depto){
            myApp::mostrarMensaje("Debe ingresar el departamento o provincia", "error", true, "mensajes");
            return false;
        }

        if (!$telefono && !$celular){
            myApp::mostrarMensaje("Debe ingresar un número teléfonico", "error", true, "mensajes");
            return false;
        }

		$user = JFactory::getUser();
		$activacion = 0;
		if (!$user->id){
			$activacion = substr(uniqid(), 1, 100);
		}
		else{
			$activacion = $user->activation;
		}

        if (!$password && !$user->id){
            myApp::mostrarMensaje("Debe ingresar la contraseña", "error", true, "mensajes");
            return false;
        }
		
		$usuarioEmail = $modelo->getUserJoomla(0, "", $email);
        if ($usuarioEmail["id"] != $user->id){
            myApp::mostrarMensaje("El correo electrónico ingresado ya se encuentra en uso", "error", true, "mensajes");
			return;
        }

		if ($password){
			$salt = JUserHelper::genRandomPassword(32);
        	$crypt = JUserHelper::getCryptedPassword($password, $salt);
        	$passwordCr = $crypt.':'.$salt;
		}
		else{
			$passwordCr = $user->password;
		}

		$exito = false;
        $fecha = date('Y-m-d H:i:s');
        if ($modelo->guardarUserJoomla($user->id, trim($nombre." ".$apellido), $username, $email, $passwordCr, $grupo["title"], 0, 0, $fecha, $fecha, $activacion)){
            $id = $user->id ? $user->id : $modelo->insertId();
            if ($modelo->guardarUser($id, $nombre, $apellido, $cedula, $direccion, $telefono, $celular, $idPais, $ciudad, $depto)){
            	if (!$user->id){
                	if ($modelo->guardarUsuarioGrupo($id, $grupo["id"])){
						$exito = true;
						$this->tmplVars["urlImagenes"] = JUri::root()."components/com_my_catalogo/images/";
						$this->tmplVars["urlSitio"] = JUri::root();
						$this->tmplVars["activacion"] = $activacion; 
						$this->tmplVars["idUsuario"] = $id; 
						$mensaje = $this->renderStr("mensajeCorreo");
                    	$sesion =& JFactory::getSession();
				    	$sesion->clear("comprando");
						$user = &JFactory::getUser();
					
						$jcfg = new JConfig();
						$mail =& JFactory::getMailer();
						$mail->addRecipient($email);
						$mail->setSender(array($jcfg->mailfrom, $jcfg->fromname));
						$mail->setSubject("Creacion y activacion de cuenta");
						$mail->IsHTML(1);	
						$mail->setBody($mensaje);
						$mail->Send();
                		//$this->login($username, $password, $redirect);
					}
                }
				else{
					$exito = true;
				}
            }
        }

		if ($exito){
			if ($user->id){
				myApp::mostrarMensaje("Su perfil de usuario ha sido guardado exitosamente","mensaje", true, "mensajes");	
			}
			else{
				myApp::mostrarMensaje("Su perfil de usuario ha sido creado exitosamente. En breve, recibirá un mensaje en su correo electrónico para validar sus datos.","mensaje", true, "mensajes");
			}			
		}		
		else{
			myApp::mostrarMensaje("No se pudo crear el perfil de usuario","error", true, "mensajes");	
		}
		
		$this->render("blanco");
    }

	function perfilUsuario(){
        $juser = JFactory::getUser();

		if (!$idUsuario = $this->request->getVar("id", 0, "int")){
			$idUsuario = $juser->id;
		}

		$usuario = $this->modelo->getUser($idUsuario);

		$_tmpl = $juser->id == $idUsuario ? "formPerfilUsuario" : "mostrarPerfilUsuario";

		if (!sizeof($usuario)){
		  $usuario = array();
		}

        $usuario["email"] = $juser->email;
        $usuario["username"] = $juser->username;
		$this->tmplVars["usuario"] = $usuario;
		$this->tmplVars["jusuario"] = $juser;
		$this->tmplVars["paises"] = $this->modelo->obtenerListaPaises();

		$this->render($_tmpl."");		
	}
	
	function activarUsuario(){
		$idUsuario = $this->request->getVar("idUsuario");
		$token = $this->request->getVar("token");
		
		$user = JFactory::getUser($idUsuario);
		if (!$user->id){
			myApp::mostrarMensaje("El usuario no se encuentra registrado","error", true, "mensajes");
			return false;
		}		
		
		if ($user->activation == 0){
			myApp::mostrarMensaje("El usuario ya se encuentra activo","error", true, "mensajes");
			return false;
		}
		
		if ($user->activation != $token){
			myApp::mostrarMensaje("El código de verificación es incorrecto","error", true, "mensajes");
			return false;
		}
		
		if ($this->modelo->activarUsuario($idUsuario)){
			myApp::mostrarMensaje("Su usuario ha sido activado exitosamente. Ahorá podrá disfrutar de todos los servicios que ofrece el sitio. Inicie sesión para continuar.","mensaje", true, "mensajes");
		}
		else{
			myApp::mostrarMensaje("No se pudo activar el usuario","error", true, "mensajes");
		}
		
		$this->render("blanco");

	}
}
?>