<?php
class com_my_catalogo extends myComponente{
	// public function procesarTarea($tarea = ""){
		// if (!sizeof($tarea))
			// $tarea = JRequest::getVar("task");

		// $metodo = JRequest::getString("metodo", "");

		// switch ($tarea){
			// case "listaColorRef":{
				// return
			// }
			// default: {
				// return parent::procesarTarea($tarea);
			// }
		// }
	// }

	var $componenteCarrito;

	function __construct($componente, $helper){
	    $cfg = new myConfig();
		$doc = myApp::getDocumento();
		$doc->incluirJQuery();
		$doc->incluirLibJs("fancybox", array("fancybox"));
        //$doc->incluirLibJs("jqzoom", array("jqzoom"));
        //$doc->incluirLibJs("easyzoom", array("easyzoom"));
        $doc->addEstilo(JUri::root()."myCore/css/gumby.css");
        //JHtmlBootstrap::loadCss();
        $componenteCarrito = $cfg->componenteCarrito;
		parent::__construct($componente, $helper);
		$this->addFuncionTmpl("menuCategorias", array($this, "menuCategorias"));		
		$this->addFuncionTmpl("getImagenReferencia", array($this, "getImagenReferencia"));
        $this->addFuncionTmpl("getAtributos", array($this, "getAtributos"));
        $this->addFuncionTmpl("getAtributosDet", array($this, "getAtributosDet"));
        $this->addFuncionTmpl("getColores", array($this, "getColores"));
        $this->addFuncionTmpl("getTallas", array($this, "getTallas"));
        $this->tmplVars["helperCarrito"] = $componenteCarrito;    
	}

	function mostrarIndex(){
        $this->listaReferencias();
	}
	
	function getImagenReferencia($idReferencia){
		$ret = "";		
		$img = $this->modelo->getListaImagenesRef($idReferencia, 1);
		if (sizeof($img)){
			$ret = $img[0]["archivo"];
		}
		return $ret;
	}
    
	function getAtributos($idReferencia){			
		$ret = $this->modelo->getListaAtributosRef($idReferencia, "S");
		return $ret;
	}
    
    function getAtributosDet($idReferencia){			
		$ret = $this->modelo->getListaAtributosRef($idReferencia);
		return $ret;
	}
    
    function getColores($idReferencia){
        $ret = $this->modelo->getListaColoresExt($idReferencia);
        return $ret;
    }
    
    function getTallas($idReferencia, $idColor){
        $ret = $this->modelo->getListaExtensiones($idReferencia, $idColor);
        return $ret;
    }

	function menuCategorias($idCategoria=0, $tarea=""){    
		$principales = $idCategoria ? false : true;
		$idCategoriaParam = (int)$this->request->getVar("idCategoria");
		$catParam = $this->modelo->getCategoria($idCategoriaParam);
		$this->tmplVars["task"] = $tarea;
		$this->tmplVars["display"] = !$principales && $idCategoria != $idCategoriaParam && $idCategoria != $catParam["id_cat"] ? "style=\"display: none\"" : ""; 
		$this->tmplVars["nodo"] = $principales ? "nodoPadre" : "nodoHijo";		
		$this->tmplVars["listaCategorias"] = $this->modelo->getListaCategorias($principales, $idCategoria);
		return $this->renderStr("menuCategoria");
	}	

    function listaReferencias(){
        $idCategoria = $this->request->getVar("idCategoria", 0, "int");
        $tipo = $this->request->getVar("tipo", "");
		
        $this->tmplVars["cat"] = $this->modelo->getCategoria($idCategoria);
        $this->tmplVars["imgBase"] = JUri::root()."myImagenes/referencias";
        $this->tmplVars["listaReferencias"] = $this->modelo->getListaReferencias($idCategoria, "N", false, $tipo);
        $this->tmplVars["menuCategoria"] = $this->menuCategorias(0, "listaReferencias");
        $this->render("listaReferencias");
    }
    
    function listaNovedades($imprimir=true){
        $this->tmplVars["imgBase"] = JUri::root()."myImagenes/referencias";
        $this->tmplVars["listaReferencias"] = $this->modelo->getListaReferencias(0, "N", true);
        
        if ($imprimir){
            $this->render("listaReferencias");
        }
        else{
            return $this->renderStr("listaReferencias");
        }        
    }    

   function detalleReferencia(){
        $idReferencia = $this->request->getVar("idReferencia", 0, "int");
        $ref = $this->modelo->getReferencia($idReferencia);

        if (sizeof($ref)){
        	$this->tmplVars["galeriaReferencia"] = $this->mostrarGaleriaReferencia($idReferencia, false);
            
			$listaReferencias = array();
            if ($ref["tipo"] == "N"){
                $listaReferencias = array($ref);
            }
            else{
                $listaReferencias = $this->modelo->getListaConjuntoRef($idReferencia);
            }
            
            $this->tmplVars["listaReferencias"] = $listaReferencias;
			$this->tmplVars["imgBase"] = JUri::root()."myImagenes/referencias";						
            
            $this->render("detalleReferencia");            
        }
        else{
            myApp::mostrarMensaje("Referencia", "error");
        }
    }
   
    function mostrarGaleriaReferencia($idReferencia=0, $imprimir=true){
        if (!$idReferencia){
            $idReferencia = $this->request->getVar("idReferencia", 0, "int");
        }

        $this->tmplVars["listaImg"] = $this->modelo->getListaImagenesRef($idReferencia);
        $this->tmplVars["imgBase"] = JUri::root()."myImagenes/referencias/".$idReferencia;        
        
        if ($imprimir){
            $this->render("galeriaArticulo");
        }
        else{
            return $this->renderStr("galeriaArticulo");
        }
    }
}
?>