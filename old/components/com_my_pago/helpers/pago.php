<?php
class com_my_pago extends myComponente{
    var $pedido = null;
    
    public function procesarTarea($tarea = ""){
        if (!sizeof($tarea))
            $tarea = $this->request->getVar("task");

        $tareas = array("datosEnvio", "mostrarResumenPedido", "realizarPago", "mostrarFormDireccion", "guardarDireccion");
        if (in_array($tarea, $tareas)){
            $sesion =& JFactory::getSession();
            $idProducto = $sesion->get("id_producto");
            
            if (!$idProducto){
                myApp::mostrarMensaje("No ha seleccionado un producto para su compra.", "alert", true, "mensajes");
                return;
            }
            
            $this->pedido = $this->obtenerPedidoActual();
            $fecha = strtotime(date('Y-m-d H:i:s'));
            $fechaPedido = strtotime($this->pedido["fecha"]);
            $dif = ($fecha - $fechaPedido) / 3600; //tiempo en horas
                   
            if ($this->pedido["estado"] == "P" && $dif < 24){
                myApp::mostrarMensaje("Ya tiene un pedido pendiente esperando respuesta de la plataforma de pagos. Intentelo nuevamente cuando se haya recibido dicha respuesta.", "alert", true, "mensajes");
                return;
            }
        }

		$_msj = $this->request->getVar("_msj");
		$_tipo = $this->request->getVar("_tipoMsj", "mensaje");
		if ($_msj){
			myApp::mostrarMensaje($_msj, $_tipo, true, "mensajes");
		}
        
        return parent::procesarTarea($tarea);
    }    

	function __construct($componente="", $helper=""){
	    $cfg = new myConfig();
		$doc = myApp::getDocumento();        
		$doc->incluirJQuery();
		$doc->incluirLibJs("colorbox", array($cfg->tmplColorBox));
		parent::__construct($componente, $helper);
	}
    
    function guardarLink(){
        $sesion =& JFactory::getSession();
        $id = $this->request->getVar("id");
        $sesion->set($id, "Y");
    }
    
    function limpiarSesion(){
        $sesion =& JFactory::getSession();
        $sesion->clear("id_pais");
        $sesion->clear("direccion");
        $sesion->clear("ciudad");
        $sesion->clear("depto");
        $sesion->clear("id_cc");
        $sesion->clear("nombre");
        $sesion->clear("apellido");
        $sesion->clear("correo");
        $sesion->clear("id_producto");
    }

	function mostrarIndex(){	    
        $this->datosEnvio();
	}
    
    function comprarProducto(){
        $idProducto = $this->request->getVar("id_producto");
        $producto = $this->modelo->getReferencia($idProducto);
        $this->limpiarSesion();
        
        if (sizeof($producto)){
            $sesion =& JFactory::getSession();
            $sesion->set("id_producto", $idProducto);
            myApp::redirect("index.php?option=".$this->nombreComp."&helper=pago&task=datosEnvio");
        }
        else{
            myApp::mostrarMensaje("El producto que intenta comprar no existe", "alert", true, "mensajes");
            return;
        }

    }

	function datosEnvio(){
		$user =& JFactory::getUser();        
        if ($user->id){
            $myUser = $this->modelo->getUser($user->id);
            
            if (sizeof($myUser)){
                $this->tmplVars["usuario"] = $myUser;
                $this->tmplVars["direccion"] = $myUser["direccion"]." - ".$myUser["ciudad"]." (".$myUser["depto"].", ".$myUser["nombre_pais"].")";
                $this->render("datosEnvio.html");
            }
            else{
                //$this->mostrarFormDireccion();
                $this->tmplVars["paises"] = $this->modelo->obtenerListaPaises();
                $this->render("formDireccionNoRegistrado.html");
            }
        }
        else{            
            $this->tmplVars["paises"] = $this->modelo->obtenerListaPaises();
            $this->render("formDireccionNoRegistrado.html");
        }
	}

	function mostrarFormDireccion(){
	    $user =& JFactory::getUser();
        $this->tmplVars["usuario"] = $this->modelo->getUser($user->id);
		$this->tmplVars["paises"] = $this->modelo->obtenerListaPaises();
        $this->render("formDireccion.html");
	}

    function guardarDireccion(){        
        $idUser = $this->request->getVar("id_user");
        $direccion = $this->request->getVar("direccion");
        $idPais = $this->request->getVar("idPais");
        $ciudad = $this->request->getVar("ciudad");
        $depto = $this->request->getVar("depto");
		
        $mensaje = "";
        $tipo = "";
		
        if (!$ciudad){
            myApp::mostrarMensaje("Debe ingresar la ciudad", "error", true, "mensajes");
            return false;
        }
		
        if (!$depto){
            myApp::mostrarMensaje("Debe ingresar el departamento o provincia", "error", true, "mensajes");
            return false;
        }
        
        $user =& JFactory::getUser();

        if ($user->id){
            $myUser = $this->modelo->getUser($user->id);
            if (sizeof($myUser)){
                if ($this->modelo->guardarDireccion($idUser, $direccion, $idPais, $ciudad, $depto)){
                    $mensaje = "Dirección guardada exitosamente";
                    $tipo = "mensaje";
                }
                else{
                    $mensaje = "No se pudo guardar la dirección";
                    $tipo = "error";
                }
            }
            else{
                $cedula = $this->request->getVar("id_cc");
                $nombre = $this->request->getVar("nombre");
                $apellido = $this->request->getVar("apellido");
                $correo = $this->request->getVar("correo");
            
                if ($this->modelo->guardarUser($user->id, $nombre, $apellido, $cedula, $direccion, "", "", $idPais, $ciudad, $depto)){
                    $mensaje = "Dirección guardada exitosamente";
                    $tipo = "mensaje";
                }
                else{
                    $mensaje = "No se pudo guardar la dirección";
                    $tipo = "error";
                }
            }
            
            myApp::redirect("index.php?option=".$this->nombreComp."&helper=pago&task=datosEnvio&_msj=".$mensaje."&_tipoMsj=".$tipo);
        }
        else{
            $cedula = $this->request->getVar("id_cc");
            $nombre = $this->request->getVar("nombre");
            $apellido = $this->request->getVar("apellido");
            $correo = $this->request->getVar("correo");
             
            if (!$cedula){
                myApp::mostrarMensaje("Debe ingresar la cedula", "error", true, "mensajes");
                return false;
            }
            
            if (!$nombre){
                myApp::mostrarMensaje("Debe ingresar su nombre", "error", true, "mensajes");
                return false;
            }
            
            if (!$apellido){
                myApp::mostrarMensaje("Debe ingresar su apellido", "error", true, "mensajes");
                return false;
            }
            
            if (!$correo){
                myApp::mostrarMensaje("Debe ingresar su direccion de correo", "error", true, "mensajes");
                return false;
            }
            
            $sesion =& JFactory::getSession();
            $sesion->set("id_pais", $idPais);
            $sesion->set("direccion", $direccion);
            $sesion->set("ciudad", $ciudad);
            $sesion->set("depto", $depto);
            $sesion->set("id_cc", $cedula);
            $sesion->set("nombre", $nombre);
            $sesion->set("apellido", $apellido);
            $sesion->set("correo", $correo);
            
            myApp::redirect("index.php?option=".$this->nombreComp."&helper=pago&task=mostrarResumenPedido");
        }
    }

    function mostrarResumenPedido(){
        $this->tmplVars["pedido"] = $this->pedido;        
        $this->render("resumenPedido.html");        
    }

    function realizarPago(){        
        $modelo = $this->modelo;                
        $pedido = $this->pedido;

        if ($modelo->guardarPedido(0, $pedido["id_user"], $pedido["id_cc"], $pedido["nombre"], $pedido["apellido"], $pedido["direccion"], $pedido["correo"], $pedido["cargo_envio"], $pedido["subtotal"], $pedido["porc_iva"], $pedido["valor_iva"], $pedido["valor_base"], $pedido["valor_venta"], $pedido["fecha"], $pedido["estado"])){
            $id = $modelo->insertId();
            $pedido["id"] = $id;
            $sesion =& JFactory::getSession();
            $idReferencia = $sesion->get("id_producto");
            
            $modelo->guardarDetallePedido($id, $idReferencia, 1, $pedido["porc_iva"], $pedido["valor_iva"], $pedido["valor_base"], $pedido["valor_venta"]);
            //$sesion->set("id_pedido", $id);
        }

        $cfg = new myConfig();
		$_plataforma = $cfg->plataformaPagos;

		if ($_plataforma){
            $modelo->cambiarEstadoPedido($pedido["id"], "P");
	        $plataforma = myPlataformaPago::cargarPlataforma($_plataforma);
	        $plataforma->realizarPago($pedido);
		}
		else{
			$this->tmplVars["urlImagenes"] = JUri::root()."components/com_my_pago/images/";
			//$this->tmplVars["mensajeCorreo"] = base64_decode($sesion->get("resumenPedido"));
			$mensaje = $this->renderStr("mensajeCorreo.html");
		    
			$user = &JFactory::getUser();
			$jcfg = new JConfig();
			$mail =& JFactory::getMailer();
			$mail->addRecipient($user->email);
			$mail->addRecipient($cfg->correoAdmin);
			$mail->setSender(array($jcfg->mailfrom, $jcfg->fromname));
			$mail->setSubject("Pedido recibido");
			$mail->IsHTML(1);
			$mail->setBody($mensaje);
			$mail->Send();
			$modelo->cambiarEstadoPedido($pedido["id"], "P");
			$this->render("mensajePedidoGuardado.html");
			//myApp::mostrarMensaje("Su pedido ha sido guardado en nuestra base de datos", "mensaje");
		}
    }

    function respuestaPago(){
        $cfg = new myConfig();
        $plataforma = myPlataformaPago::cargarPlataforma($cfg->plataformaPagos);
        $ret = $plataforma->respuestaPago($this->request->get("GET"));
        $this->limpiarSesion();
        //$this->procesarRespuestaPagos($ret);
        myApp::redirect("index.php", $ret["mensaje"]);
    }

    function confirmacionPago(){
        $cfg = new myConfig();
        $plataforma = myPlataformaPago::cargarPlataforma($cfg->plataformaPagos);
        $ret = $plataforma->confirmacionPago($this->request->get("POST"));
        $this->procesarRespuestaPagos($ret);
        $this->limpiarSesion();
    }
    
    function procesarRespuestaPagos($ret){
        $modelo = $this->modelo;

        if ($ret["error"]){
            //myApp::mostrarMensaje($ret["error"], "error", true, "mensajes");
            myApp::redirect("index.php", $ret["error"]);
        }
        else{
            $pedido = $modelo->getPedido($ret["id_pedido"]);
            $pedido["detalle"] = $modelo->getListaDetallePedido($ret["id_pedido"]);
            if (sizeof($pedido)){
                $tituloMensaje = "";
                $mensajeEstado = "";
                
                switch ($ret["respuesta"]){
                    case "A":{
                        $modelo->cambiarEstadoPedido($pedido["id"], "A");
                        $modelo->asignarTransPedido($pedido["id"], $ret["trans"]);
                        $tituloMensaje = "Transaccion exitosa";
                        $mensajeEstado = "Hemos recibido su pedido correctamente, en breve nos pondremos en contacto con usted";
                        break;
                    }
                    case "P":{
                        $modelo->cambiarEstadoPedido($pedido["id"], "P");
                        $tituloMensaje = "Transaccion pendiente por aprobación";
                        $mensajeEstado = "Su transacción se encuentra pendiente por aprobación de parte de la entidad de pagos en linea";
                        break;
                    }
                    case "R":
                    case "C":{
                        $modelo->cambiarEstadoPedido($pedido["id"], "R");
                        $tituloMensaje = "Transaccion rechazada";
                        $mensajeEstado = "Su transacción ha sido rechazada por la entidad de pagos en linea";
                        break;
                    }
                }
                
                $this->tmplVars["titulo"] = $tituloMensaje;
                $this->tmplVars["mensajeEstado"] = $mensajeEstado;
                $this->tmplVars["pedido"] = $pedido;
                $this->tmplVars["urlImagenes"] = JUri::root()."components/com_my_pago/images/";
                $this->tmplVars["pedido"] = $pedido;
                $mensaje = $this->renderStr("mensajeCorreo.html");

                $jcfg = new JConfig();
                $mail =& JFactory::getMailer();
                $mail->addRecipient($pedido["correo"]);
                $mail->addRecipient($jcfg->correoAdmin);
                $mail->setSender(array($jcfg->mailfrom, $jcfg->fromname));
                $mail->setSubject("Pedido recibido");
                $mail->IsHTML(1);
                $mail->setBody($mensaje);
                $mail->Send();			
            
            }
            else{
                myApp::mostrarMensaje("Pedido no encontrado", "error", true, "mensajes");
            }

			myApp::redirect("index.php", $ret["mensaje"]);
        }
    }

	function obtenerPedidoActual(){
		$modelo = $this->modelo;
		$user =& JFactory::getUser();
        $userJoomla = null;
		$cfg = new myConfig();
        
        $direccionPedido = "";
        $cedulaCliente = "";
        $nombreCliente = "";
        $apellidoCliente = "";
        $correoCliente = "";
        $idProducto = "";
        
        if ($user->id){
            $userJoomla = $modelo->getUser($user->id);
        }
        
        if (sizeof($userJoomla)){
            $direccionPedido = $userJoomla["direccion"]." ".$userJoomla["ciudad"]."(".$userJoomla["depto"]." - ".$userJoomla["nombre_pais"].")";
            $cedulaCliente = $userJoomla["id_cc"];
            $nombreCliente = $userJoomla["nombre"];
            $apellidoCliente = $userJoomla["apellido"];
            $correoCliente = $user->email;
        }
        else{
            $sesion =& JFactory::getSession();
            $pais = $modelo->getPais($sesion->get("id_pais"));
            $direccionPedido = $sesion->get("direccion")." ".$sesion->get("ciudad")."(".$sesion->get("depto")." - ".$pais["nombre"].")";
            $cedulaCliente = $sesion->get("id_cc");
            $nombreCliente = $sesion->get("nombre");
            $apellidoCliente = $sesion->get("apellido");
            $correoCliente = $sesion->get("correo");
            $idProducto = $sesion->get("id_producto");
        }
        
        $pedido = $modelo->getListaPedidos($user->id, $cedulaCliente, "", 0, 0, true, true);
        
        $retPedido = null;
        
        if (sizeof($pedido)){
            if ($pedido[0]["estado"] == "N" || $pedido[0]["estado"] == "P"){
                $retPedido = $pedido[0];
                $retPedido["detalle"] = $this->modelo->getListaDetallePedido($retPedido["id"]);
            }
        }
        
        if(!sizeof($retPedido)){
            $producto = $modelo->getReferencia($idProducto);
            
            $valorProducto = (double)$producto["valor_base"];
            $subtotal = $valorProducto + (double)$cfg->gastosEnvio;
            $valorBase = $subtotal / (1 + (double)$cfg->porcIva / 100);
            $valorIva = $subtotal - $valorBase;
            
            $retPedido = array(
                "id" => 0,
                "id_user" => $user->id,
                "id_cc" => $cedulaCliente,
                "nombre" => $nombreCliente,
                "apellido" => $apellidoCliente,
                "direccion" => $direccionPedido,
                "correo" => $correoCliente,
                "cargo_envio" => $cfg->gastosEnvio,
                "porc_iva" => $cfg->porcIva,
                "valor_iva" => $valorIva,
                "valor_base" => $valorBase,
                "subtotal" => $valorProducto,
                "valor_venta" => $subtotal,
                "fecha" => date('Y-m-d H:i:s'),
                "estado" => "N",
                "cod_trans" => "",
                "num_guia" => ""
            );
                                    
            $valorBase = $valorProducto / (1 + (double)$cfg->porcIva / 100);
            $valorIva = $valorProducto - $valorBase;
            
            $detalle = array(
                "id" => $idProducto,
                "nombre_referencia" => $producto["nombre"],
                "cantidad" => 1,
                "porc_iva" => $cfg->porcIva,
                "valor_iva" => $valorIva,
                "valor_base" => $valorBase,
                "valor_venta" => $valorProducto                
            );
            
            $retPedido["detalle"][] = $detalle;
        }
        
        return $retPedido;
	}
}
?>