(function(window, $){
    function hookFormDireccion(){
        $("a[rel='guardarDireccion']").unbind("click");
        $("a[rel='guardarDireccion']").bind("click", function (e){
            e.preventDefault();        
            formDireccion.submit();
        });
    }
    
	function inicioDoc(){
		$("a[rel='formDireccion']").colorbox({onComplete: function() {  
				hookFormDireccion();
			} 
		});
        
        hookFormDireccion();
        
		var envioPago = $("#enviarPago");
		if (envioPago !== null){
			if (envioPago.val() === "Y"){
				formPago.submit();
			}
		}
	}	
	$(document).ready(inicioDoc);
})(window, jQuery);