(function(window, $){
    function hookFormContacto(){
        $(".form_plano input[type='button']").unbind("click");
        $(".form_plano input[type='button']").bind("click", function(e) {
            e.preventDefault();
            
            if ($("#nombre").val() === ""){
                alert('Debe ingresar su nombre');
                return;
            }

            if ($("#tel").val() === ""){
                alert('Debe ingresar su número de teléfono');
                return;
            }
            
            if ($("#email").val() === ""){
                alert('Debe ingresar su dirección de correo electrónico');
                return;
            }
            
            $("#loader").addClass("loading");
            var $form = $("#" + $(this).attr("data-form"));
            $.ajax({
                url : $form.attr("action"),
                data: $form.serialize(),
                method: 'post',
                success: function(res){
                    $("#" + $(this).attr("data-form") + " input[type='text'], #" + $(this).attr("data-form") + " textarea").each(function() {
                        $(this).val('');
                    });
                    $("#loader").removeClass("loading");
                    alert(res);
                }
            });
        });
    }
    
    function hookFormCompra(){
        $("select#depto").change(function(e){
            var $ciudad = $("select#id_ciudad");
            $.ajax({
                url: 'index.php?option=com_my_component&controller=carrito&task=obtenerCiudades&format=raw',
                data: {
                    id_depto : $(this).val()    
                },
                success: function (res) {
                    var json = $.parseJSON(res);
                    $ciudad.empty();
                    $.each(json, function(i, o) {
                        $ciudad.append("<option value='" + o.id + "'>" + o.nombre + "</div>");
                    });
                }
            });
        });
        
        $("#form_compra").submit(function (e) {
            var continuar = 1;
            if ($("#nombre").val() === ''){
                alert('Debe ingresar su nombre');
                continuar = 0;
            }
            
            /*if ($("#edad_usuario").val() === ''){
                alert('Debe ingresar su edad');
                continuar = 0;
            }*/
            
            if ($("#email").val() === ''){
                alert('Debe ingresar su dirección de correo');
                continuar = 0;
            }
            
            if ($("#telefono").val() === '' && $("#celular").val() === ''){
                alert('Debe ingresar su número de teléfono y/o de celular');
                continuar = 0;
            }
            
            if ($("#ciudad").val() === ''){
                alert('Debe seleccionar la ciudad de envío');
                continuar = 0;
            }
            
            if ($("#direccion").val() === ''){
                alert('Debe ingresar la dirección de envío');
                continuar = 0;
            }
            
            if ($("#id_forma_pago").val() === ''){
                alert('Debe seleccionar la modalidad de pago');
                continuar = 0;
            }
            
            if (continuar === 0){
                e.preventDefault();
            }
            
            return;
        });
    }
    
    function hookModuloCarrito() {
        $("a[rel='condiciones_carrito']").featherlight();
        
        $("a[rel='pedido']").unbind("click");
        $("a[rel='pedido']").bind("click", function(e) {
            e.preventDefault();
            var $a = $(this);
            $.ajax({
                url: 'index.php?option=com_my_component&controller=carrito&task=obtenerNumItems&format=raw',
                method: 'get',
                success: function(res){
                    if (res > 0){
                        $.featherlight($a.attr("href"),
                        {                            
                            type:'ajax',
                            beforeOpen: function() {
                                
                            },
                            afterOpen: function() {
                                hookFormCompra();
                            }
                        });
                    }
                    else{
                        alert('Su carrito de compras se encuentra vacío. Por favor agregue algún producto en él.');
                    }
                }
            });
        });
        
        /*$("a[rel='pedido']").featherlight(
        { 
            type:'ajax',
            beforeOpen: function() {
                
            },
            afterOpen: function() {
                hookFormCompra();
            }
        });*/
        
        $("#modCarrito a.boton-cantidad").click(function (e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr("href"),
                method: 'get',
                success: function (res) {
                    $("#modCarrito").fadeOut(200);
                    $("#modCarrito").html(res).fadeIn(200);                    
                    hookModuloCarrito();
                }
            });
        });
    }
    
    function hookListaItems(){        
        $("a[rel='lnkQuitarItem']").click(function(e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr("href"),
                data: {
                    format: 'raw'
                },
                success: function(res){
                    $("#divItemscarrito").html(res);
                    hookListaItems();
                }
            });
        });
        
        $("a[rel='lnkActualizarItem']").click(function(e) {
            e.preventDefault();
            var $dataReg = $(this).attr("data-reg");
            var $cantidad = $($dataReg).val();
            $.ajax({
                url: $(this).attr("href"),
                data: {
                    format: 'raw',
                    cantidad: $cantidad
                },
                success: function(res){
                    $("#divItemscarrito").html(res);
                    hookListaItems();
                }
            });
        });
    }
    
    function scrollTo(target) {
        var wheight = $(window).height();
        
        var ooo = $(target).offset().top;
        $('html, body').animate({scrollTop:ooo}, 600);
    }
    
    $(document).ready(function() {
        $(window).resize(function() {                
            if ($("#top-menu-resp").is(":visible")){
                $("#header-top ul:first-child").hide();
            }
            else{                    
                $("#header-top ul:first-child").show();
            }
        });

        $("#top-menu-resp > a").click(function(e){
            e.preventDefault();
            $("#header-top ul:first-child").toggle("slow");
        });
        
        $("#my_slider_2 a").featherlight();
        
       $("img[rel='thumbImagen']").click(function (e) {
            e.preventDefault();
            $(".imgThumbActiva").removeClass("imgThumbActiva");
            $(this).addClass("imgThumbActiva");
            $("#idProducto").attr("href", $(this).attr("data-img"));
            $("#idProducto > img").attr("src", $(this).attr("src"));
        });
        
        $("a.lnkColor").click(function(e) {
            e.preventDefault();
            $("a.lnkColor.active").each(function(i, o){
                $(this).removeClass("active");
            });
            
            $(this).addClass("active");
            $("#idExt").val($(this).attr("data-ext"));
        });

        $("a[rel='lnkAgregarProd']").click(function (e) {
            e.preventDefault();
            $cantidad = $("#cantidad").val();
            $idExt = $("#idExt").val();
            $idProd = $("#id").val();

            $.ajax({
                url: 'index.php?option=com_my_component&controller=carrito&task=agregarProducto',
                method: 'post',
                data: { id_producto: $idProd, id_ext: $idExt, cantidad: $cantidad, format: 'raw' },
                success: function (res) {
                    $("#modCarrito").fadeOut(200);
                    $("#modCarrito").html(res).fadeIn(200);                    
                    hookModuloCarrito();
                }
            });
        });
        
        hookFormContacto();
        hookListaItems();
        hookModuloCarrito();        

        $(document).foundation();
	});
})(window, jQuery);