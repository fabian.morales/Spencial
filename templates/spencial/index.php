<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add Stylesheets
$doc->addScript(JURI::root(true).'/media/jui/js/jquery.min.js');
$doc->addStyleSheet('templates/'.$this->template.'/foundation/css/foundation.min.css');
$doc->addStyleSheet(JURI::root(true).'/myCore/css/foundation/fonts/foundation-icons.css');
$doc->addScript('templates/'.$this->template.'/foundation/js/foundation.min.js');
$doc->addScript('templates/'.$this->template.'/js/lightSlider/jquery.lightSlider.min.js');
$doc->addScript('templates/'.$this->template.'/js/fancybox/fancybox.js');
$doc->addScript('myCore/js/featherlight/featherlight.js');
$doc->addStyleSheet('myCore/js/featherlight/featherlight.css');
$doc->addStyleSheet('templates/'.$this->template.'/js/lightSlider/lightSlider.css');
$doc->addStyleSheet('templates/'.$this->template.'/js/fancybox/fancybox.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/style.css');
$doc->addScript('templates/'.$this->template.'/js/scripts.js');
$doc->addScript('myCore/js/catalogo.js');
$doc->addScript('myCore/js/carrito.js');
// Add current user information
$user = JFactory::getUser();
?>
<?php if(!$params->get('html5', 0)): ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php else: ?>
	<?php echo '<!DOCTYPE html>'; ?>
<?php endif; ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	   
	<jdoc:include type="head" />

	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
	<![endif]-->
</head>
<body>
    <div id="top-link"></div>    
    <div id="loader"><div></div></div>    
    <!-- Home -->
    <div id="header-top">
        <div class="row">
            <div class="small-12 medium-4 columns small-only-text-center">
                <img class="logo" src="templates/<?php echo $this->template; ?>/img/logo.png" />
            </div>
            <div class="small-12 medium-8 columns">
                <div class="row">
                    <div class="small-12 medium-6 columns">&nbsp;</div>
                    <div class="small-12 medium-6 columns">
                        <div class="row">
                            <jdoc:include type="modules" name="redes-header" style="xhtml" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 columns small-only-text-center medium-text-right">
                        <div id="top-menu-resp" class="menu-resp">
                            <div>Menu</div>
                            <a href="#"></a>
                        </div>
                        <jdoc:include type="modules" name="top-menu" style="xhtml" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row collapse fullWidth relativo">
        <div class="modulo-carrito" id="modCarrito">
            <jdoc:include type="modules" name="modulo-carrito" style="xhtml" />
        </div>    
        <div class="small-12 columns">
            <div id='carga_mod'></div>
            <jdoc:include type="modules" name="banner" style="xhtml" />
        </div>
    </div>
    <div class="row collapse fullWidth relativo">
        <div class="small-12 medium-5 columns">
            <jdoc:include type="modules" name="custom1" style="xhtml" />
        </div>
        <div class="small-12 medium-7 columns">
            <jdoc:include type="modules" name="custom2" style="xhtml" />
        </div>

    </div>
	<jdoc:include type="modules" name="slider" style="xhtml" />
    <div class="franja_gris">
        <div class="row">
            <jdoc:include type="modules" name="sub-banner" style="xhtml" />
        </div>
    </div>
    <jdoc:include type="modules" name="contenido" style="xhtml" />
    <div class="row">
        <div class="small-12 columns">
            <jdoc:include type="message" />
        </div>
    </div>
    <?php        
        $menu = $app->getMenu();
        if ($menu->getActive() != $menu->getDefault()) : ?>
        <div class="row">
            <div class="large-12 columns component">
                <jdoc:include type="component" />
            </div>
        </div>
    <?php endif; ?>
    <footer>
        <div class="row text-center">
            <jdoc:include type="modules" name="footer" style="xhtml" />
        </div>
    </footer>
</body>
</html>
