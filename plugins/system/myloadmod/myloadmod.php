<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  System.sef
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Joomla! SEF Plugin
 *
 * @package     Joomla.Plugin
 * @subpackage  System.sef
 * @since       1.5
 */
require_once(JPATH_ROOT."/myCore/autoload.php");

class plgSystemMyloadmod extends JPlugin
{

	var $cargar = false;
    
	public function onAfterRender()
	{
        if ($this->cargar){
            $app = JFactory::getApplication();

    		if ($app->getName() != 'site' || $app->getCfg('sef') == '0')
    		{
    			return true;
    		}
            $buffer = JResponse::getBody();
            
            $c = myApp::getController("catalogo");
            $texto = $c->mostrarSliderIndividual();
            $buffer = str_replace("<div id='carga_mod'></div>", $texto, $buffer);
            
            /*$document =& JFactory::getDocument();
            $renderer = $document->loadRenderer('module');
            $modulos = JModuleHelper::getModules("manuales");        
            $options = array('style' => 'raw');
            $module	= JModuleHelper::getModule('mod_custom', 'Videotutoriales y manuales de uso');        
            echo $renderer->render($module, $options);*/

            
    		JResponse::setBody($buffer);
        }
		return true;
	}
    
    public function onCargaModulo(){
        $this->cargar = true;
        return true;
    }
}
