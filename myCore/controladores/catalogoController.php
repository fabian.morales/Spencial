<?php

class catalogoController extends myController{
	function index(){
        return $this->listarProductos();
	}
    
    function mostrarAyuda(){
        return "";
    }
    
    function mostrarSliderProductos(){
        $request = myApp::getRequest();
        $idCategoria = $request->getVar("id_cat", 0, "int");
        $productos = [];
        $categoria = null;

        if (!empty($idCategoria)){
            $categoria = CategoriaRef::where("id", $idCategoria)->with(["productos.extensiones.color", "productos.extensiones.talla", "productos.atributosRef.atributo", "productos.imagenes" => function($query){
                $query->where("tipo", "ban")->orWhere("tipo", "bn2");
            }])->first();
            
            if (sizeof($categoria)){
                $productos = $categoria->productos;
            }
        }

        if (!sizeof($productos)){
            $productos = Producto::with(["extensiones.color", "extensiones.talla", "atributosRef.atributo", "imagenes" => function($query){
                $query->where("tipo", "ban")->orWhere("tipo", "bn2");
            }])->get();
        }

        return myView::render("catalogo.slider_productos", ["productos" => $productos, "urlImg" => myApp::urlImg(), "categoria" => $categoria]);        
    }

    function listarProductos(){
        $request = myApp::getRequest();
        $idCategoria = $request->getVar("id_cat", 0, "int");
        $productos = [];
        $categoria = null;

        if (!empty($idCategoria)){
            $categoria = CategoriaRef::where("id", $idCategoria)->with(["productos.extensiones.color", "productos.extensiones.talla", "productos.atributosRef.atributo", "productos.imagenes" => function($query){
                $query->where("tipo", "cat");
            }])->first();
            if (sizeof($categoria)){
                $productos = $categoria->productos;
            }
        }

        if (!sizeof($productos)){
            $productos = Producto::with(["extensiones.color", "extensiones.talla", "atributosRef.atributo", "imagenes" => function($query){
                $query->where("tipo", "cat");
            }])->get();
        }

        return myView::render("catalogo.lista_productos", ["productos" => $productos, "urlImg" => myApp::urlImg(), "categoria" => $categoria]);        
    }
        
    function mostrarProducto(){
        $doc = myApp::getDocumento();
        $doc->addScript(JUri::root()."media/jui/js/jquery.min.js");
        $doc->incluirLibJs("featherlight", ["featherlight"]);
        $doc->addScript(JUri::root()."myCore/js/catalogo.js");
        $idProd = myApp::getRequest()->getVar("id", 0, "int");
        
        $producto = Producto::where("id", $idProd)->with(["extensiones.color", "extensiones.talla", "atributosRef.atributo", "imagenes" => function($query){
            $query->where("tipo", "gal");
        }])->first();
        
        $cat = $producto->categorias()->with(["productos.imagenes" => function($query){
            $query->where("tipo", "min");
        }])->first();

        if (sizeof($producto)){                        
		    /*$listaReferencias = array();
            if ($ref["tipo"] == "N"){
                $listaReferencias = array($ref);
            }
            else{
                $listaReferencias = $this->modelo->getListaConjuntoRef($idReferencia);
            }*/
            JPluginHelper::importPlugin( 'myloadmod' );
            $dispatcher = JEventDispatcher::getInstance();
            $dispatcher->trigger('onCargaModulo');
            return myView::render("catalogo.detalle_producto", ["producto" => $producto, "urlImg" => myApp::urlImg(), "cat" => $cat]);
        }
        else{
            myApp::redirect("index.php?option=com_my_component&controller=catalogo", "Producto no encontrado");
        }
    }
    
    public function mostrarSliderIndividual(){
        $idProd = myApp::getRequest()->getVar("id");
        $sliders = ImagenRef::where("id_referencia", $idProd)->where(function($query) {
            $query->where("tipo", "ban");
            $query->orWhere("tipo", "bn2");
        })->get();
        
        return myView::render("catalogo.slider_individual", ["id_referencia" => $idProd, "urlImg" => myApp::urlImg(), "sliders" => $sliders]);
    }
    
    public function mostrarPoliticas(){
        $db = myApp::getEloquent();
        $doc = $db::table('content')
                ->select('introtext', 'fulltext')
                ->where('id', '4')
                ->first();
                
        return myView::render("catalogo.politicas", ["articulo" => $doc]);
    }
}
?>