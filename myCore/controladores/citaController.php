<?php

class citaController extends myController{
    public function index(){
        return "";
    }
    
    public function mostrarForm(){
        return myView::render("cita.form");
    }
    
    public function enviarCorreoContacto(){
        $contacto = array();
        $contacto["nombre"] = myApp::getRequest()->getVar("nombre");
        $contacto["tel"] = myApp::getRequest()->getVar("tel");        
        $contacto["email"] = myApp::getRequest()->getVar("email");
        $contacto["observaciones"] = myApp::getRequest()->getVar("observaciones");
        $contacto["asunto"] = 'Solicitud de contacto';
        $contacto["url"] = JUri::root();
        
        $jcfg = new JConfig();        
        $cfg = new myConfig();
        $mail = JFactory::getMailer();
        $mail->addRecipient($jcfg->mailfrom);
        //$mail->addBCC($cfg->correoAdmin);
        $mail->setSender(array($jcfg->mailfrom, $jcfg->fromname));
        $mail->setSubject("Solicitud de contacto");
        $mail->addBCC("desarrollo@encubo.ws");
        $mail->addBCC("gerencia@andresmesa.co");
        $mail->IsHTML(1);
        $mail->setBody(myView::render("cita.correo", array("contacto" => $contacto)));
        $envio = $mail->Send();
        
        if ( $envio !== true ) {
            return 'Ha ocurrido un error enviando el correo: '.$envio->message;
        } 
        else {
            return 'Se ha envido un correo con la solicitud de contacto';
        }
    }
}