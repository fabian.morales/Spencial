<?php
class usuarioController extends myController{
    function index(){
        $paises = Pais::all();
        $estadosCorp = [];
        $estadosFact = [];
        $jusuario = JFactory::getUser();
        $compania = new Compania();
        if ($jusuario->id){
            $compania = Compania::where("id_usuario", $jusuario->id)->first();
            $estadosCorp = Departamento::where("id_pais", $compania->id_pais_corp)->get();
            $estadosFact = Departamento::where("id_pais", $compania->id_pais_fact)->get();
        }
        
        return myView::render("usuario.form_usuario", ["compania" => $compania, "jusuario" => $jusuario, "paises" => $paises, "estadosCorp" => $estadosCorp, "estadosFact" => $estadosFact]);
    }
    
    function mostrarLogin($redirect = ""){
        if (empty($redirect)){
            $redirect = myApp::getRequest()->getVar("redirect", "", "base64");    
        }
        else{
            $redirect = base64_encode($redirect);
        }
        
        $usuario = JFactory::getUser();
        if (!$usuario->id){
            return myView::render("usuario.form_login", ["redirect" => $redirect]);
        }
        else{
            return $this->index();
        }
    }

    function login($username="", $password="", $redirect="", $noredir = false){  
        $req = myApp::getRequest();
        if (!$redirect){
            if ($redirect = $req->getVar("redirect", "", "base64")) {
                $redirect = base64_decode($redirect);
                if (!JURI::isInternal($redirect)) {
                    $redirect = '';
                }
            }
        }

        if (!$username){
            $username = $req->getVar('username_login', '', 'username');
        }

        if (!$password){
            $password = $req->getVar('password_login', '', "RAW");
        }        

        $opciones = array();
        $opciones['remember'] = false;
        $opciones['return'] = $redirect;

        $credenciales = array();
        $credenciales['username'] = $username;
        $credenciales['password'] = $password;		
        $error = myApp::login($credenciales, $opciones);
				
        if(!JError::isError($error)){
            if (!$noredir){
                if (!$redirect) {
                    $redirect = 'index.php?option=com_my_component&controller=usuario';
                }
                
                myApp::redirect($redirect);                
            }
        }
        else{        	
            myApp::mostrarMensaje($error->message, "error");
        }
    }

    function logout($noredir = false){
        $error = myApp::logout();
        $req = myApp::getRequest();
        if (!$noredir){
            if(!JError::isError($error)){
                if ($redirect = $req->getVar('redirect', '', 'base64')){
                    $redirect = base64_decode($redirect);
                    if (!JURI::isInternal($redirect)) {
                        $redirect = 'index.php';
                    }
                }
                else{
                    $redirect = 'index.php';
                }

                if ($redirect && !(strpos($redirect, 'com_my_component'))){
                    myApp::redirect($redirect);
                }
            }
            else{
                myApp::redirect('index.php?option=com_my_component&controller=usuario');
            }            
        }
    }

    function guardarUsuario(){
        jimport('joomla.user.helper');
        $modelo = myApp::getModelo();
        $request = myApp::getRequest();
        
        $nuevo = false;
        $fecha = date('Y-m-d H:i:s');
        
        $usuario = new Usuario();
        $compania = new Compania();
        $usuarioJoomla = JFactory::getUser();
        if ($usuarioJoomla->id){
            $usuario = Usuario::find($usuarioJoomla->id);
            $compania_aux = Compania::where("id_usuario", $usuarioJoomla->id)->first();
            if (sizeof($compania_aux)){
                $compania = $compania_aux;
            }
        }
        else{
            $usuario->name = $request->getVar("nombre");            
            $usuario->activation = substr(uniqid(), 1, 100);
            $usuario->registerDate = $fecha;
            $usuario->params = '';
            $nuevo = true;
        }
        
        $passwordOrig = $usuario->password;
        
        $usuario->username = $request->getVar("email");
        $usuario->lastvisitDate = $fecha;
        $password = $request->getVar('password', '', JREQUEST_ALLOWRAW);
        $usuario->fill($request->all());       
        $compania->fill($request->all());
        
        if (is_numeric($compania->id_estado_corp)){
            $cnt = Departamento::where("id", $compania->id_estado_corp)->count();
            if ($cnt == 0){
                $compania->estado_corp = $compania->id_estado_corp;
                $compania->id_estado_corp = null;
            }
        }
        else{
            $compania->estado_corp = $compania->id_estado_corp;
            $compania->id_estado_corp = null;
        }
        
        if (is_numeric($compania->id_estado_fact)){
            $cnt = Departamento::where("id", $compania->id_estado_fact)->count();
            if ($cnt == 0){
                $compania->estado_fact = $compania->id_estado_fact;
                $compania->id_estado_fact = null;
            }
        }
        else{
            $compania->estado_fact = $compania->id_estado_fact;
            $compania->id_estado_fact = null;
        }
        
        $grupo = $modelo->getGrupoUser();

        if ($redirect = $request->getVar('_redirect', '', 'base64')) {
            $redirect = base64_decode($redirect);
            if (!JURI::isInternal($redirect)) {
                $redirect = '';
            }
        }
        
        if (!$password && !$usuarioJoomla->id){
            myApp::mostrarMensaje("Enter a password", "error");
            return false;
        }
		
        $usuarioEmail = Usuario::where("email", $usuario->email)->first();
        
        if (sizeof($usuarioEmail) && $usuarioEmail->id != $usuarioJoomla->id){
            myApp::mostrarMensaje("The email address is already in use", "error");
            return;
        }

        if ($password){
            $salt = JUserHelper::genRandomPassword(32);
            $crypt = JUserHelper::getCryptedPassword($password, $salt);
            $usuario->password = $crypt.':'.$salt;
        }
        else{
            $usuario->password = $passwordOrig;
        }
        
        $exito = false;                
        if ($usuario->save()){
            $compania->id_usuario = $usuario->id;
            
            if ($compania->save()){
            	if ($nuevo){
                    if ($modelo->guardarUsuarioGrupo($usuario->id, $grupo["id"])){
                        $exito = true;
                        $urlImagenes = JUri::root()."images/";
                        $mensaje = myView::render("usuario.correo_cuenta_nueva", ["urlImagenes" => $urlImagenes, "urlSitio" => JUri::root(), "usuario" => $usuario, "compania" => $compania]);

                        $jcfg = new JConfig();
                        $mail =& JFactory::getMailer();
                        $mail->addRecipient($usuario->email);
                        $mail->setSender(array($jcfg->mailfrom, $jcfg->fromname));
                        $mail->setSubject("New account");
                        $mail->IsHTML(1);	
                        $mail->setBody($mensaje);
                        $mail->Send();
                    }
                }
                else{
                    $exito = true;
                }
            }
        }

        if ($exito){
            if ($usuario->id){
                myApp::mostrarMensaje("Your user account was updated successfully","message");	
            }
            else{
                myApp::mostrarMensaje("Your user account was created successfully. Shortly, you will receive a email message to validate you account.","message");
            }			
        }		
        else{
            myApp::mostrarMensaje("Your user account was not created","error");	
        }

        return myView::render("usuario.blanco");
    }
	
    function activarUsuario(){
        $idUsuario = myApp::getRequest()->getVar("idUsuario");
        $token = myApp::getRequest()->getVar("token");

        $user = Usuario::find($idUsuario);
        if (!$user->id){
            myApp::mostrarMensaje("This account is not valid", "error");
            return false;
        }		

        if ($user->activation == 0){
            myApp::mostrarMensaje("This account is already activated", "error");
            return false;
        }

        if ($user->activation != $token){
            myApp::mostrarMensaje("The validation code is incorrect", "error");
            return false;
        }

        $user->activation = 0;
        
        if ($user->save()){
            myApp::mostrarMensaje("Your account was activated successfully", "message");
        }
        else{
            myApp::mostrarMensaje("Your account was not activated", "error");
        }

        return myView::render("usuario.blanco");
    }
}
?>