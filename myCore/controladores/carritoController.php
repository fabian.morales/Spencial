<?php

class carritoController extends myController{
    var $idSesion;
    
    function carritoController(){
        $this->idSesion = $this->obtenerIdSesion();
    }
    
    function obtenerIdSesion(){
        $sesion = JFactory::getSession();
        $idSesion = $sesion->get("id_sesion");
               
        if (empty($idSesion)){
            $idSesion = uniqid();
            $sesion->set("id_sesion", $idSesion);            
        }
        return $idSesion;
    }

    function guardarIdSesion($id){
        $sesion = JFactory::getSession();
        $sesion->set("id_sesion", $id);
    }

    public function index(){
        $sesion = JFactory::getSession();
        $pedido = unserialize($sesion->get("pedido"));
        if (sizeof($pedido) && !empty($pedido)){
            $doc = myApp::getDocumento();
            $doc->addScript(JUri::root()."media/jui/js/jquery.min.js");
            $doc->incluirLibJs("featherlight", ["featherlight"]);
            $doc->addScript(JUri::root()."myCore/js/carrito.js");        
            $carrito = Carrito::where("id_sesion", $this->idSesion)->with(["extension.producto", "extension.producto.imagenes" => function($query) { 
                $query->where("tipo", "gal");
            }, "extension.talla", "extension.color"])->get();
            $totales = Carrito::totales($this->idSesion);
            
            $myCfg = new myConfig();
            $envio = $pedido->id_ciudad == 169 ? 0 : $myCfg->gastosEnvio;
            return myView::render("carrito.index", ["urlImg" => myApp::urlImg(), "carrito" => $carrito, "totales" => $totales, "envio" => $envio]);
        }
        else{
            myApp::redirect("index.php", "No ha diligenciado el formulario de compras");
        }
    }

    public function listarCarrito(){    
        $sesion = JFactory::getSession();
        $pedido = unserialize($sesion->get("pedido"));
        if (sizeof($pedido) && !empty($pedido)){    
            $carrito = Carrito::where("id_sesion", $this->idSesion)->with(["extension.producto", "extension.producto.imagenes" => function($query) {
                $query->where("tipo", "gal");
            }, "extension.talla", "extension.color"])->get();
            $totales = Carrito::totales($this->idSesion);
            $myCfg = new myConfig();
            $envio = $pedido->id_ciudad == 169 ? 0 : $myCfg->gastosEnvio;
            return myView::render("carrito.lista_items", ["urlImg" => myApp::urlImg(), "carrito" => $carrito, "totales" => $totales, "envio" => $envio]);
        }
    }

    public function agregarProducto(){        
        $request = myApp::getRequest();
        $idProducto = $request->getVar("id_producto");
        $idExt = $request->getVar("id_ext");
        $cantidad = $request->getVar("cantidad", 0, "int");
        $ext = Extension::find($idExt);

        if (sizeof($ext)){
            $reg = Carrito::where("id_sesion", $this->idSesion)->where("id_ext", $idExt)->first();            
            if (!sizeof($reg)){
                if ($cantidad > 0){
                    $reg = new Carrito();
                    $reg->id_sesion = $this->idSesion;
                    $reg->id_ext = $idExt;
                    $reg->id_referencia = $idProducto;
                    $reg->cantidad = 0;
                    $reg->fecha = date('Y-m-d H:i:s');

                    $user = JFactory::getUser();
                    if ($user->id){
                        $reg->id_usuario = $user->id;
                    }
                    
                    $reg->cantidad += (int)$cantidad;
                    $reg->save();
                }
            }
            else{
                $reg->cantidad += (int)$cantidad;
                $reg->save();
                
                if ($cantidad < 0){
                    Carrito::where("id_sesion", $this->idSesion)->where("cantidad", "<=", 0)->delete();
                }
            }
        }

        return $this->mostrarTotalesModulo();
    }
    
    public function actualizarItem(){
        $id = myApp::getRequest()->getVar("id");        
        $item = Carrito::where("id_sesion", $this->idSesion)->where("id", $id)->first();
        if (sizeof($item)){
            $item->cantidad = (int)myApp::getRequest()->getVar("cantidad");
            $item->save();
            Carrito::where("id_sesion", $this->idSesion)->where("cantidad", "<=", 0)->delete();
        }
        
        return $this->listarCarrito();
    }
    
    public function quitarItem(){
        $id = myApp::getRequest()->getVar("id");        
        Carrito::where("id_sesion", $this->idSesion)->where("id", $id)->delete();
        return $this->listarCarrito();
    }

    public function mostrarTotalesModulo(){
        $doc = myApp::getDocumento();
        $doc->addScript(JUri::root()."myCore/js/catalogo.js");        
        $carrito = Carrito::where("id_sesion", $this->idSesion)->with(["extension.producto.imagenes" => function($query) {
            $query->where("tipo", "gal");
        }, "extension.talla", "extension.color"])->get();
        $totales = Carrito::totales($this->idSesion);
        $myCfg = new myConfig();
        return myView::render("carrito.modulo", ["urlImg" => myApp::urlImg(), "carrito" => $carrito, "totales" => $totales, "envio" => $myCfg->gastosEnvio]);
    }
    
    public function obtenerNumItems(){        
        $carrito = Carrito::where("id_sesion", $this->idSesion)->count();
        return (int)$carrito;
    }
    
    public function mostrarFormCompra(){        
        $carrito = Carrito::where("id_sesion", $this->idSesion)->count();
        if ((int)$carrito == 0){
            myApp::redirect("index.php", "Su carrito de compras se encuentra vacío. Por favor agregue algún producto en él.");
        }
        
        $deptos = Departamento::orderBy("nombre", "asc")->get();
        $formas = FormaPago::all();
        return myView::render("carrito.form_compra", ["deptos" => $deptos, "formas" => $formas]);
    }
    
    public function obtenerCiudades(){
        $idDepto = myApp::getRequest()->getVar("id_depto");
        $ciudades = Ciudad::where("id_depto", $idDepto)->get();
        return json_encode($ciudades);
    }
    
    public function realizarPedido(){
        $cfg = new myConfig();
        $iva = $cfg->porcIva;
        $envio = $cfg->gastosEnvio;
        
        $pedido = new Pedido();
        $pedido->fill(myApp::getRequest()->all());
        $pedido->porc_iva = $iva;
        $pedido->fecha = date('Y-m-d H:i:s');
        $pedido->cargo_envio = $pedido->id_ciudad == 169 ? 0 : $envio;
        $pedido->estado = 'N';
        
        $sesion = JFactory::getSession();
        $sesion->set("pedido", serialize($pedido));
        
        return $this->index();
    }
    
    public function guardarPedido(){
        $db = myApp::getEloquent();
        $pdo = $db::connection()->getPdo();
        $pdo->beginTransaction();
        $cfg = new myConfig();
        $iva = $cfg->porcIva;
        
        try
        {
            $sesion = JFactory::getSession();
            $pedido = unserialize($sesion->get("pedido"));
                   
            if (!sizeof($pedido)){
                myApp::redirect("index.php", "No ha diligenciado el formulario de compras");
            }
        
            if ($pedido->save()){                
                $carrito = Carrito::where("id_sesion", $this->idSesion)->with(["extension.producto"])->get();
                $valorItems = 0;
                $valorIva = 0;
                
                foreach ($carrito as $c){
                    $detalle = new DetallePedido();
                    $detalle->id_pedido = $pedido->id;
                    $detalle->id_referencia = $c->extension->producto->id;
                    $detalle->id_ext = $c->extension->id;
                    $detalle->cantidad = $c->cantidad;
                    $detalle->valor = $c->extension->producto->valor_base * $c->cantidad;
                    $detalle->porc_iva = $iva;
                    $detalle->valor_iva = $detalle->valor - ($detalle->valor / (1 + $iva / 100));
                    
                    $valorItems += $detalle->valor;
                    $valorIva += $detalle->valor_iva;
                    
                    if (!$detalle->save()){
                        throw new \Exception('No se pudo guardar el detalle del pedido');
                        myApp::redirect("index.php", "No se pudo guardar el detalle del pedido");
                    }
                }
                
                $pedido->valor_items = $valorItems;
                $pedido->valor_iva = $valorIva;
                $pedido->valor_total = $valorItems + $pedido->cargo_envio;
                
                if (!$pedido->save()){
                    throw new \Exception('No se pudo finalizar el pedido');
                    myApp::redirect("index.php", "No se pudo finalizar el pedido");
                }
                
                $carrito = Carrito::where("id_sesion", $this->idSesion)->delete();
                $pdo->commit();
                $sesion->clear("pedido");
                $sesion->destroy();
                
                $jcfg = new JConfig();                        
                $mail = JFactory::getMailer();
                $mail->addRecipient($jcfg->mailfrom);
                $mail->addRecipient($pedido->email);
                //$mail->addBCC($cfg->correoAdmin);
                $mail->setSender(array($jcfg->mailfrom, $jcfg->fromname));
                $mail->setSubject("Pedido nuevo - Spencial");
                $mail->addBCC("desarrollo@encubo.ws");        
                $mail->addBCC("gerencia@andresmesa.co");
                $mail->IsHTML(1);
                $mail->setBody($this->generarCorreo($pedido->id));
                $envio = $mail->Send();                
                
                if ( $envio !== true ) {
                    myApp::redirect("index.php", "Se ha realizado tu pedido exitosamente, pero no se pudo enviar el correo");
                } 
                else {
                    myApp::redirect("index.php", "Se ha realizado tu pedido exitosamente. Se ha enviado a tu correo los detalles del mismo");
                }                
            }
            else{
                throw new \Exception('No se pudo crear el pedido');
                myApp::redirect("index.php", "No se crear el pedido");
            }
        }catch(Exception $e){            
            $pdo->rollback();
            myApp::redirect("index.php", "No se pudo realizar el pedido");
        }
    }
    
    public function generarCorreo($idPedido=""){
        if (empty($idPedido)){
            $idPedido = myApp::getRequest()->getVar("id");
        }
        
        $pedido = Pedido::where("id", $idPedido)->with(["detalle.extension.producto", "detalle.extension.talla", "detalle.extension.color", "ciudad", "formaPago"])->first();
        return myView::render("carrito.correo_pedido", ["pedido" => $pedido, "url" => JUri::root(), "asunto" => "Pedido nuevo - Spencial"]);
    }
}