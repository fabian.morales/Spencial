<?php

class myView {
    private static $twig;
    
    public static function boot(){
        Twig_Autoloader::register();
        
        $dirTemplates = [];

        $app = JFactory::getApplication();
        if (!$app->isAdmin()){
            $template = Template::where("client_id", 0)->where("home", 1)->first();
            if (sizeof($template)){
                $dirTemplate = JPATH_THEMES.DS.$template->template.DS."html".DS."my_vistas".DS;
                if (is_dir($dirTemplate)){
                    $dirTemplates[] = $dirTemplate;
                }
            }
        }
        
        $dirTemplates[] = dirname(__DIR__).DS."vistas".DS;
        
        $loaderTwig = new Twig_Loader_Filesystem($dirTemplates);
        myView::$twig = new Twig_Environment($loaderTwig, array("cache" => false, "debug" => true));
        
        $filter = new Twig_SimpleFunction("trad", array(myView, "traducir"));        
        myView::$twig->addFunction($filter);
        
        myView::$twig->addExtension(new Twig_Extension_Debug());
        
        $lang = dirname(__DIR__)."/trad/".myApp::getLang().".php";
        if (is_file($lang)){
            include_once($lang);
        }
    }
    
    public static function render($vista, $vars = []){        
        $vista = str_replace(".", "/", $vista).".twig";        
        return myView::$twig->render($vista, $vars);
    }
	
    public static function renderPdf($vista, $vars, $archivo){
        $html = myView::render($vista, $vars);

        /*$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);        
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('FrontierSoft');
        
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetFont('helvetica', '', 11);

        $pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);        

        $pdf->AddPage('P', 'Letter');
                
        $bMargin = $pdf->getBreakMargin();        
        $auto_page_break = $pdf->getAutoPageBreak();        
        $pdf->SetAutoPageBreak(false, 0);
        $pdf->SetAutoPageBreak(false, $bMargin);        
        $pdf->setPageMark();
        $pdf->writeHTML($html, true, false, true, false, '');
        
        $pdf->Output($archivo, "F");*/
        $estilosFou = file_get_contents(dirname(__DIR__)."/css/foundation/css/foundation.css");
        $estilosPdf = file_get_contents(dirname(__DIR__)."/css/pdf.css");
        
        $mpdf = new mPDF();
        $mpdf->WriteHTML($estilosFou, 1);
        $mpdf->WriteHTML($estilosPdf, 1);
		$mpdf->WriteHTML($html, 2);
		$mpdf->Output($archivo, "F");
    }
    
    public static function traducir($contenido, $seccion){ 
        $key = $seccion."_".md5($contenido);
        global $_trad;
        
        if (array_key_exists($key, $_trad)){
            return ($_trad[$key]);   
        }
        else{
            return $contenido;
        }
    }
    
	public static function adicionarFuncion($alias, $funcion){
		$filter = new Twig_SimpleFilter($alias, $funcion);
		myView::$twig->addFilter($filter);
	}
}
