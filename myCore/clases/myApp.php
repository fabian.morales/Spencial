<?php
/*
	Héctor Fabián Morales Ramírez
	Tecnólogo en Ingeniería de Sistemas
	Enero 2011
*/
use \Illuminate\Database\Capsule\Manager as Capsule;

class myApp{
    static $document;
    static $modelo;
    static $func;	
    static $request;
    static $eloquent;

    public static function getController($nombre = ""){
        if (empty($nombre)){
            $req = myApp::getRequest();
            $nombre = $req->getVar("controller");
        }
        
        $rutaController = dirname(__DIR__)."/controladores/".$nombre."Controller.php";
        
        $claseController = $nombre."Controller";
        if (is_file($rutaController)){
            require_once($rutaController);
        }
        
        return new $claseController();
    }
    
    public static function redirect($url, $mensaje=""){
        $app = JFactory::getApplication();
        $app->redirect($url, $mensaje);
    }    

    public static function login($credenciales, $opciones){
        $app = JFactory::getApplication();
        return $app->login($credenciales, $opciones);
    }

    public static function logout(){
        $app = JFactory::getApplication();
        return $app->logout();
    }

    public static function getModelo(){
        if (!myApp::$modelo){
            myApp::$modelo = new myModelo();
        }
        return myApp::$modelo;
    }
    
    public static function getEloquent(){
        if (!myApp::$eloquent){
            $cfg = new JConfig();
            myApp::$eloquent = new Capsule;
            myApp::$eloquent->addConnection(array(
                    'driver'    => 'mysql',
                    'host'      => $cfg->host,
                    'database'  => $cfg->db,
                    'username'  => $cfg->user,
                    'password'  => $cfg->password,
                    'charset'   => 'utf8',
                    'collation' => 'utf8_unicode_ci',
                    'prefix'    => $cfg->dbprefix
            ));
            
            myApp::$eloquent->setAsGlobal();
            myApp::$eloquent->bootEloquent();
        }
        
        return myApp::$eloquent;
    }

    public static function getDocumento(){
        if (!myApp::$document){
            myApp::$document = new myDocumento();
        }
        return myApp::$document;
    }

    public static function getFunciones(){
        if (!myApp::$func){
            myApp::$func = new myFunciones();
        }
        return myApp::$func;
    }

    public static function getRequest(){
        if (!myApp::$request){
            myApp::$request = new myRequest();
        }
        return myApp::$request;
    }

    public static function getLang(){
        $sesion = JFactory::getSession();
        $lang = $sesion->get("myLang", "es");
        return $lang;
    }

    public static function setLang($lang){
        $sesion =& JFactory::getSession();
        $lang = $sesion->set("myLang", $lang);
    }

    public static function pathImg(){
        return JPATH_ROOT.DS."myImagenes";
    }

    public static function urlImg(){
        return JURI::root()."myImagenes/";
    }

    public static function mostrarMensaje($mensaje, $tipo=""){
        JFactory::getApplication()->enqueueMessage(JText::_($mensaje), $tipo);
        return $mensaje;
    }
}