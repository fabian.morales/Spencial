create table spen15_my_pais(
	id integer not null auto_increment,
	nombre varchar(60),
	primary key (id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;

create table spen15_my_categoria(
	id integer not null auto_increment,
	nombre varchar(60),
	id_cat integer,
	primary key(id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;

create table spen15_my_referencia(
	id integer not null auto_increment,
	nombre varchar(60),
	referencia varchar(20),
	descripcion varchar(500),	
	valor_base numeric(10,3),
	fecha timestamp,
	existencias integer default 0,
    tipo char(1) default 'N',
	primary key (id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;

create table spen15_my_conjuntoref(
	id_conjunto integer,
	id_referencia integer,
	foreign key (id_conjunto) references spen15_my_referencia (id) on delete cascade,
	foreign key (id_referencia) references spen15_my_referencia (id) on delete cascade,
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;

create table spen15_my_cat_ref(
	id_categoria integer,
	id_referencia integer,
	foreign key (id_referencia) references spen15_my_referencia (id),
	foreign key (id_categoria) references spen15_my_categoria (id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 	
);

create table spen15_my_color(
	id integer not null auto_increment,
	nombre varchar(60),
	codigo varchar(10),
	thumb varchar(255),
	primary key (id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;

create table spen15_my_talla(
	id integer not null auto_increment,
	nombre varchar(60),
	abrev varchar(10),
	primary key (id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;

create table spen15_my_extension(
	id integer not null auto_increment,
	id_talla integer default null,
	id_color integer default null,
	id_referencia integer not null,
	mod_valor numeric(10, 3),
	thumb varchar(255),
	primary key (id),
	foreign key (id_referencia) references spen15_my_referencia (id) on delete cascade,
	foreign key (id_talla) references spen15_my_talla (id),
	foreign key (id_color) references spen15_my_color (id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;

create table spen15_my_atributo(
	id integer not null auto_increment primary key,
	portada enum('S', 'N') default 'N',
	descripcion varchar(60) not null,
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
);

create table spen15_my_atributo_ref(
	id integer not null auto_increment primary key,
	id_atributo integer not null,
	id_referencia integer not null,
	valor varchar(300) default ' ',
	foreign key (id_atributo) references spen15_my_atributo (id) on delete cascade,
	foreign key (id_referencia) references spen15_my_referencia (id) on delete cascade,
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
);

CREATE TABLE spen15_my_img_ref (
	id INT(11) NOT NULL AUTO_INCREMENT,
	id_referencia INT(11) NULL DEFAULT NULL,	
	descripcion VARCHAR(60) NULL DEFAULT NULL,
	archivo VARCHAR(255) NULL DEFAULT NULL,
	tipo varchar(5),
	PRIMARY KEY (id),
	FOREIGN KEY (id_referencia) REFERENCES spen15_my_referencia (id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE=InnoDB;

create table spen15_my_carrito(
	id integer not null auto_increment primary key,
	id_user integer default 0 not null,
	id_sesion varchar(255) default ' ' not null,
	id_referencia integer,	
	id_ext integer DEFAULT NULL,
	cantidad integer default 0 not null,
	fecha timestamp default current_timestamp,
	foreign key (id_referencia) references spen15_my_referencia (id) on delete cascade,
	foreign key (id_ext) references spen15_my_extension (id) on delete cascade,
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;

create table spen15_my_user(
	id integer not null,
	id_cc varchar(15),
	nombre varchar(255),
	apellido varchar(255),
	direccion varchar(512),
	telefono varchar(20),
	celular varchar(20),
	id_pais int,
	ciudad varchar(60),
	depto varchar(60),	
	primary key(id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;

create table spen15_my_forma_pago(
	id integer not null auto_increment,	
	nombre varchar(255),
	primary key(id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;

create table spen15_my_pedido(
	id integer not null auto_increment,
	id_user integer not null,
	nombre varchar(200),
	edad_usuario int,
	direccion varchar(512),	
	id_ciudad int,
	celular varchar(20),
	telefono varchar(20),
	email varchar(512),
	id_forma_pago int,
	regalo char(1) default 'N',
	observaciones varchar(512),
	num_items integer not null,
	cargo_envio numeric(10,3) default 0 not null,
	valor_items numeric(10,3) default 0 not null,
	valor_total numeric(10,3) default 0 not null,
	porc_iva numeric(10,3) default 0 not null,
	valor_iva numeric(10,3) default 0 not null,
	fecha timestamp,
	estado enum('N', 'R', 'A', 'P', 'E') default 'N',
	cod_trans varchar(255),	
	primary key (id),
	foreign key (id_user) references spen15_my_user (id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;


CREATE TABLE spen15_my_detalle_pedido (
	id integer not null auto_increment,
	id_pedido INT(11) NOT NULL DEFAULT '0',
	id_referencia INT(11) NOT NULL,	
	id_ext INT(11),
	cantidad INT(11) NOT NULL DEFAULT '0',
	valor DECIMAL(10,3) NOT NULL DEFAULT '0.000',
	porc_iva DECIMAL(10,3) NOT NULL DEFAULT '0.000',
	valor_iva DECIMAL(10,3) NOT NULL DEFAULT '0.000',
	PRIMARY KEY (id),
	FOREIGN KEY (id_pedido) REFERENCES spen15_my_pedido (id),
	FOREIGN KEY (id_referencia) REFERENCES spen15_my_referencia (id),	
	FOREIGN KEY (id_ext) REFERENCES spen15_my_extension (id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE=InnoDB;

insert into spen15_my_pais (nombre) values ('Afganistán ');
insert into spen15_my_pais (nombre) values ('Akrotiri ');
insert into spen15_my_pais (nombre) values ('Albania ');
insert into spen15_my_pais (nombre) values ('Alemania ');
insert into spen15_my_pais (nombre) values ('Andorra ');
insert into spen15_my_pais (nombre) values ('Angola ');
insert into spen15_my_pais (nombre) values ('Anguila ');
insert into spen15_my_pais (nombre) values ('Antártida ');
insert into spen15_my_pais (nombre) values ('Antigua y Barbuda ');
insert into spen15_my_pais (nombre) values ('Antillas Neerlandesas ');
insert into spen15_my_pais (nombre) values ('Arabia Saudí ');
insert into spen15_my_pais (nombre) values ('Arctic Ocean ');
insert into spen15_my_pais (nombre) values ('Argelia ');
insert into spen15_my_pais (nombre) values ('Argentina ');
insert into spen15_my_pais (nombre) values ('Armenia ');
insert into spen15_my_pais (nombre) values ('Aruba ');
insert into spen15_my_pais (nombre) values ('Ashmore andCartier Islands ');
insert into spen15_my_pais (nombre) values ('Atlantic Ocean ');
insert into spen15_my_pais (nombre) values ('Australia ');
insert into spen15_my_pais (nombre) values ('Austria ');
insert into spen15_my_pais (nombre) values ('Azerbaiyán ');
insert into spen15_my_pais (nombre) values ('Bahamas ');
insert into spen15_my_pais (nombre) values ('Bahráin ');
insert into spen15_my_pais (nombre) values ('Bangladesh ');
insert into spen15_my_pais (nombre) values ('Barbados ');
insert into spen15_my_pais (nombre) values ('Bélgica ');
insert into spen15_my_pais (nombre) values ('Belice ');
insert into spen15_my_pais (nombre) values ('Benín ');
insert into spen15_my_pais (nombre) values ('Bermudas ');
insert into spen15_my_pais (nombre) values ('Bielorrusia ');
insert into spen15_my_pais (nombre) values ('Birmania Myanmar ');
insert into spen15_my_pais (nombre) values ('Bolivia ');
insert into spen15_my_pais (nombre) values ('Bosnia y Hercegovina ');
insert into spen15_my_pais (nombre) values ('Botsuana ');
insert into spen15_my_pais (nombre) values ('Brasil ');
insert into spen15_my_pais (nombre) values ('Brunéi ');
insert into spen15_my_pais (nombre) values ('Bulgaria ');
insert into spen15_my_pais (nombre) values ('Burkina Faso ');
insert into spen15_my_pais (nombre) values ('Burundi ');
insert into spen15_my_pais (nombre) values ('Bután ');
insert into spen15_my_pais (nombre) values ('Cabo Verde ');
insert into spen15_my_pais (nombre) values ('Camboya ');
insert into spen15_my_pais (nombre) values ('Camerún ');
insert into spen15_my_pais (nombre) values ('Canadá ');
insert into spen15_my_pais (nombre) values ('Chad ');
insert into spen15_my_pais (nombre) values ('Chile ');
insert into spen15_my_pais (nombre) values ('China ');
insert into spen15_my_pais (nombre) values ('Chipre ');
insert into spen15_my_pais (nombre) values ('Clipperton Island ');
insert into spen15_my_pais (nombre) values ('Colombia ');
insert into spen15_my_pais (nombre) values ('Comoras ');
insert into spen15_my_pais (nombre) values ('Congo ');
insert into spen15_my_pais (nombre) values ('Coral Sea Islands ');
insert into spen15_my_pais (nombre) values ('Corea del Norte ');
insert into spen15_my_pais (nombre) values ('Corea del Sur ');
insert into spen15_my_pais (nombre) values ('Costa de Marfil ');
insert into spen15_my_pais (nombre) values ('Costa Rica ');
insert into spen15_my_pais (nombre) values ('Croacia ');
insert into spen15_my_pais (nombre) values ('Cuba ');
insert into spen15_my_pais (nombre) values ('Dhekelia ');
insert into spen15_my_pais (nombre) values ('Dinamarca ');
insert into spen15_my_pais (nombre) values ('Dominica ');
insert into spen15_my_pais (nombre) values ('Ecuador ');
insert into spen15_my_pais (nombre) values ('Egipto ');
insert into spen15_my_pais (nombre) values ('El Salvador ');
insert into spen15_my_pais (nombre) values ('El Vaticano ');
insert into spen15_my_pais (nombre) values ('Emiratos Árabes Unidos ');
insert into spen15_my_pais (nombre) values ('Eritrea ');
insert into spen15_my_pais (nombre) values ('Eslovaquia ');
insert into spen15_my_pais (nombre) values ('Eslovenia ');
insert into spen15_my_pais (nombre) values ('España ');
insert into spen15_my_pais (nombre) values ('Estados Unidos ');
insert into spen15_my_pais (nombre) values ('Estonia ');
insert into spen15_my_pais (nombre) values ('Etiopía ');
insert into spen15_my_pais (nombre) values ('Filipinas ');
insert into spen15_my_pais (nombre) values ('Finlandia ');
insert into spen15_my_pais (nombre) values ('Fiyi ');
insert into spen15_my_pais (nombre) values ('Francia ');
insert into spen15_my_pais (nombre) values ('Gabón ');
insert into spen15_my_pais (nombre) values ('Gambia ');
insert into spen15_my_pais (nombre) values ('Gaza Strip ');
insert into spen15_my_pais (nombre) values ('Georgia ');
insert into spen15_my_pais (nombre) values ('Ghana ');
insert into spen15_my_pais (nombre) values ('Gibraltar ');
insert into spen15_my_pais (nombre) values ('Granada ');
insert into spen15_my_pais (nombre) values ('Grecia ');
insert into spen15_my_pais (nombre) values ('Groenlandia ');
insert into spen15_my_pais (nombre) values ('Guam ');
insert into spen15_my_pais (nombre) values ('Guatemala ');
insert into spen15_my_pais (nombre) values ('Guernsey ');
insert into spen15_my_pais (nombre) values ('Guinea ');
insert into spen15_my_pais (nombre) values ('Guinea Ecuatorial ');
insert into spen15_my_pais (nombre) values ('Guinea-Bissau ');
insert into spen15_my_pais (nombre) values ('Guyana ');
insert into spen15_my_pais (nombre) values ('Haití ');
insert into spen15_my_pais (nombre) values ('Honduras ');
insert into spen15_my_pais (nombre) values ('Hong Kong ');
insert into spen15_my_pais (nombre) values ('Hungría ');
insert into spen15_my_pais (nombre) values ('India ');
insert into spen15_my_pais (nombre) values ('Indian Ocean ');
insert into spen15_my_pais (nombre) values ('Indonesia ');
insert into spen15_my_pais (nombre) values ('Irán ');
insert into spen15_my_pais (nombre) values ('Iraq ');
insert into spen15_my_pais (nombre) values ('Irlanda ');
insert into spen15_my_pais (nombre) values ('Isla Bouvet ');
insert into spen15_my_pais (nombre) values ('Isla Christmas ');
insert into spen15_my_pais (nombre) values ('Isla Norfolk ');
insert into spen15_my_pais (nombre) values ('Islandia ');
insert into spen15_my_pais (nombre) values ('Islas Caimán ');
insert into spen15_my_pais (nombre) values ('Islas Cocos ');
insert into spen15_my_pais (nombre) values ('Islas Cook ');
insert into spen15_my_pais (nombre) values ('Islas Feroe ');
insert into spen15_my_pais (nombre) values ('Islas Georgia del Sur y Sandwich del Sur ');
insert into spen15_my_pais (nombre) values ('Islas Heard y McDonald ');
insert into spen15_my_pais (nombre) values ('Islas Malvinas ');
insert into spen15_my_pais (nombre) values ('Islas Marianas del Norte ');
insert into spen15_my_pais (nombre) values ('IslasMarshall ');
insert into spen15_my_pais (nombre) values ('Islas Pitcairn ');
insert into spen15_my_pais (nombre) values ('Islas Salomón ');
insert into spen15_my_pais (nombre) values ('Islas Turcas y Caicos ');
insert into spen15_my_pais (nombre) values ('Islas Vírgenes Americanas ');
insert into spen15_my_pais (nombre) values ('Islas Vírgenes Británicas ');
insert into spen15_my_pais (nombre) values ('Israel ');
insert into spen15_my_pais (nombre) values ('Italia ');
insert into spen15_my_pais (nombre) values ('Jamaica ');
insert into spen15_my_pais (nombre) values ('Jan Mayen ');
insert into spen15_my_pais (nombre) values ('Japón ');
insert into spen15_my_pais (nombre) values ('Jersey ');
insert into spen15_my_pais (nombre) values ('Jordania ');
insert into spen15_my_pais (nombre) values ('Kazajistán ');
insert into spen15_my_pais (nombre) values ('Kenia ');
insert into spen15_my_pais (nombre) values ('Kirguizistán ');
insert into spen15_my_pais (nombre) values ('Kiribati ');
insert into spen15_my_pais (nombre) values ('Kuwait ');
insert into spen15_my_pais (nombre) values ('Laos ');
insert into spen15_my_pais (nombre) values ('Lesoto ');
insert into spen15_my_pais (nombre) values ('Letonia ');
insert into spen15_my_pais (nombre) values ('Líbano ');
insert into spen15_my_pais (nombre) values ('Liberia ');
insert into spen15_my_pais (nombre) values ('Libia ');
insert into spen15_my_pais (nombre) values ('Liechtenstein ');
insert into spen15_my_pais (nombre) values ('Lituania ');
insert into spen15_my_pais (nombre) values ('Luxemburgo ');
insert into spen15_my_pais (nombre) values ('Macao ');
insert into spen15_my_pais (nombre) values ('Macedonia ');
insert into spen15_my_pais (nombre) values ('Madagascar ');
insert into spen15_my_pais (nombre) values ('Malasia ');
insert into spen15_my_pais (nombre) values ('Malaui ');
insert into spen15_my_pais (nombre) values ('Maldivas ');
insert into spen15_my_pais (nombre) values ('Malí ');
insert into spen15_my_pais (nombre) values ('Malta ');
insert into spen15_my_pais (nombre) values ('Man, Isle of ');
insert into spen15_my_pais (nombre) values ('Marruecos ');
insert into spen15_my_pais (nombre) values ('Mauricio ');
insert into spen15_my_pais (nombre) values ('Mauritania ');
insert into spen15_my_pais (nombre) values ('Mayotte ');
insert into spen15_my_pais (nombre) values ('México ');
insert into spen15_my_pais (nombre) values ('Micronesia ');
insert into spen15_my_pais (nombre) values ('Moldavia ');
insert into spen15_my_pais (nombre) values ('Mónaco ');
insert into spen15_my_pais (nombre) values ('Mongolia ');
insert into spen15_my_pais (nombre) values ('Montserrat ');
insert into spen15_my_pais (nombre) values ('Mozambique ');
insert into spen15_my_pais (nombre) values ('Namibia ');
insert into spen15_my_pais (nombre) values ('Nauru ');
insert into spen15_my_pais (nombre) values ('Navassa Island ');
insert into spen15_my_pais (nombre) values ('Nepal ');
insert into spen15_my_pais (nombre) values ('Nicaragua ');
insert into spen15_my_pais (nombre) values ('Níger ');
insert into spen15_my_pais (nombre) values ('Nigeria ');
insert into spen15_my_pais (nombre) values ('Niue ');
insert into spen15_my_pais (nombre) values ('Noruega ');
insert into spen15_my_pais (nombre) values ('Nueva Caledonia ');
insert into spen15_my_pais (nombre) values ('Nueva Zelanda ');
insert into spen15_my_pais (nombre) values ('Omán ');
insert into spen15_my_pais (nombre) values ('Pacific Ocean ');
insert into spen15_my_pais (nombre) values ('Países Bajos ');
insert into spen15_my_pais (nombre) values ('Pakistán ');
insert into spen15_my_pais (nombre) values ('Palaos ');
insert into spen15_my_pais (nombre) values ('Panamá ');
insert into spen15_my_pais (nombre) values ('Papúa-Nueva Guinea ');
insert into spen15_my_pais (nombre) values ('Paracel Islands ');
insert into spen15_my_pais (nombre) values ('Paraguay ');
insert into spen15_my_pais (nombre) values ('Perú ');
insert into spen15_my_pais (nombre) values ('Polinesia Francesa ');
insert into spen15_my_pais (nombre) values ('Polonia ');
insert into spen15_my_pais (nombre) values ('Portugal ');
insert into spen15_my_pais (nombre) values ('Puerto Rico ');
insert into spen15_my_pais (nombre) values ('Qatar ');
insert into spen15_my_pais (nombre) values ('Reino Unido ');
insert into spen15_my_pais (nombre) values ('República Centroafricana ');
insert into spen15_my_pais (nombre) values ('República Checa ');
insert into spen15_my_pais (nombre) values ('República Democrática del Congo ');
insert into spen15_my_pais (nombre) values ('República Dominicana ');
insert into spen15_my_pais (nombre) values ('Ruanda ');
insert into spen15_my_pais (nombre) values ('Rumania ');
insert into spen15_my_pais (nombre) values ('Rusia ');
insert into spen15_my_pais (nombre) values ('Sáhara Occidental ');
insert into spen15_my_pais (nombre) values ('Samoa ');
insert into spen15_my_pais (nombre) values ('Samoa Americana ');
insert into spen15_my_pais (nombre) values ('San Cristóbal y Nieves ');
insert into spen15_my_pais (nombre) values ('San Marino ');
insert into spen15_my_pais (nombre) values ('San Pedro y Miquelón ');
insert into spen15_my_pais (nombre) values ('San Vicente y las Granadinas ');
insert into spen15_my_pais (nombre) values ('Santa Helena ');
insert into spen15_my_pais (nombre) values ('Santa Lucía ');
insert into spen15_my_pais (nombre) values ('Santo Tomé y Príncipe ');
insert into spen15_my_pais (nombre) values ('Senegal ');
insert into spen15_my_pais (nombre) values ('Seychelles ');
insert into spen15_my_pais (nombre) values ('Sierra Leona ');
insert into spen15_my_pais (nombre) values ('Singapur ');
insert into spen15_my_pais (nombre) values ('Siria ');
insert into spen15_my_pais (nombre) values ('Somalia ');
insert into spen15_my_pais (nombre) values ('Southern Ocean ');
insert into spen15_my_pais (nombre) values ('Spratly Islands ');
insert into spen15_my_pais (nombre) values ('Sri Lanka ');
insert into spen15_my_pais (nombre) values ('Suazilandia ');
insert into spen15_my_pais (nombre) values ('Sudáfrica ');
insert into spen15_my_pais (nombre) values ('Sudán ');
insert into spen15_my_pais (nombre) values ('Suecia ');
insert into spen15_my_pais (nombre) values ('Suiza ');
insert into spen15_my_pais (nombre) values ('Surinam ');
insert into spen15_my_pais (nombre) values ('Svalbard y Jan Mayen ');
insert into spen15_my_pais (nombre) values ('Tailandia ');
insert into spen15_my_pais (nombre) values ('Taiwán ');
insert into spen15_my_pais (nombre) values ('Tanzania ');
insert into spen15_my_pais (nombre) values ('Tayikistán ');
insert into spen15_my_pais (nombre) values ('TerritorioBritánicodel Océano Indico ');
insert into spen15_my_pais (nombre) values ('Territorios Australes Franceses ');
insert into spen15_my_pais (nombre) values ('Timor Oriental ');
insert into spen15_my_pais (nombre) values ('Togo ');
insert into spen15_my_pais (nombre) values ('Tokelau ');
insert into spen15_my_pais (nombre) values ('Tonga ');
insert into spen15_my_pais (nombre) values ('Trinidad y Tobago ');
insert into spen15_my_pais (nombre) values ('Túnez ');
insert into spen15_my_pais (nombre) values ('Turkmenistán ');
insert into spen15_my_pais (nombre) values ('Turquía ');
insert into spen15_my_pais (nombre) values ('Tuvalu ');
insert into spen15_my_pais (nombre) values ('Ucrania ');
insert into spen15_my_pais (nombre) values ('Uganda ');
insert into spen15_my_pais (nombre) values ('Unión Europea ');
insert into spen15_my_pais (nombre) values ('Uruguay ');
insert into spen15_my_pais (nombre) values ('Uzbekistán ');
insert into spen15_my_pais (nombre) values ('Vanuatu ');
insert into spen15_my_pais (nombre) values ('Venezuela ');
insert into spen15_my_pais (nombre) values ('Vietnam ');
insert into spen15_my_pais (nombre) values ('Wake Island ');
insert into spen15_my_pais (nombre) values ('Wallis y Futuna ');
insert into spen15_my_pais (nombre) values ('West Bank ');
insert into spen15_my_pais (nombre) values ('World ');
insert into spen15_my_pais (nombre) values ('Yemen ');
insert into spen15_my_pais (nombre) values ('Yibuti ');
insert into spen15_my_pais (nombre) values ('Zambia ');
insert into spen15_my_pais (nombre) values ('Zimbabue ');

create table spen15_my_depto(
    id smallint not null primary key,
    nombre varchar(255) not null,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
);

create table spen15_my_ciudad(
    id smallint not null primary key,
    nombre varchar(255) not null,
    id_depto smallint not null,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
);

INSERT INTO spen15_my_depto (id, nombre) VALUES (1, 'Antioquia');
INSERT INTO spen15_my_depto (id, nombre) VALUES (2, 'Atlántico');
INSERT INTO spen15_my_depto (id, nombre) VALUES (3, 'Bogotá D.C');
INSERT INTO spen15_my_depto (id, nombre) VALUES (4, 'Bolívar');
INSERT INTO spen15_my_depto (id, nombre) VALUES (5, 'Boyacá');
INSERT INTO spen15_my_depto (id, nombre) VALUES (6, 'Caldas');
INSERT INTO spen15_my_depto (id, nombre) VALUES (7, 'Caquetá');
INSERT INTO spen15_my_depto (id, nombre) VALUES (8, 'Cauca');
INSERT INTO spen15_my_depto (id, nombre) VALUES (9, 'Cesar');
INSERT INTO spen15_my_depto (id, nombre) VALUES (10, 'Córdoba');
INSERT INTO spen15_my_depto (id, nombre) VALUES (11, 'Cundinamarca');
INSERT INTO spen15_my_depto (id, nombre) VALUES (12, 'Chocó');
INSERT INTO spen15_my_depto (id, nombre) VALUES (13, 'Huila');
INSERT INTO spen15_my_depto (id, nombre) VALUES (14, 'La Guajira');
INSERT INTO spen15_my_depto (id, nombre) VALUES (15, 'Magdalena');
INSERT INTO spen15_my_depto (id, nombre) VALUES (16, 'Meta');
INSERT INTO spen15_my_depto (id, nombre) VALUES (17, 'Nariño');
INSERT INTO spen15_my_depto (id, nombre) VALUES (18, 'Nte de Santander');
INSERT INTO spen15_my_depto (id, nombre) VALUES (19, 'Quindio');
INSERT INTO spen15_my_depto (id, nombre) VALUES (20, 'Risaralda');
INSERT INTO spen15_my_depto (id, nombre) VALUES (21, 'Santander');
INSERT INTO spen15_my_depto (id, nombre) VALUES (22, 'Sucre');
INSERT INTO spen15_my_depto (id, nombre) VALUES (23, 'Tolima');
INSERT INTO spen15_my_depto (id, nombre) VALUES (24, 'Valle del Cauca');
INSERT INTO spen15_my_depto (id, nombre) VALUES (25, 'Arauca');
INSERT INTO spen15_my_depto (id, nombre) VALUES (26, 'Casanare');
INSERT INTO spen15_my_depto (id, nombre) VALUES (27, 'Putumayo');
INSERT INTO spen15_my_depto (id, nombre) VALUES (28, 'Archipiélago de San Andrés, Providencia y Santa Catalina');
INSERT INTO spen15_my_depto (id, nombre) VALUES (29, 'Amazonas');
INSERT INTO spen15_my_depto (id, nombre) VALUES (30, 'Guainía');
INSERT INTO spen15_my_depto (id, nombre) VALUES (31, 'Guaviare');
INSERT INTO spen15_my_depto (id, nombre) VALUES (32, 'Vaupés');
INSERT INTO spen15_my_depto (id, nombre) VALUES (33, 'Vichada');


INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1, 29, 'LETICIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (2, 29, 'EL ENCANTO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (3, 29, 'LA CHORRERA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (4, 29, 'LA PEDRERA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (5, 29, 'LA VICTORIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (6, 29, 'MIRITI - PARANA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (7, 29, 'PUERTO ALEGRIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (8, 29, 'PUERTO ARICA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (9, 29, 'PUERTO NARIÑO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (10, 29, 'PUERTO SANTANDER');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (11, 29, 'TARAPACA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (12, 1, 'MEDELLIN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (13, 1, 'ABEJORRAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (14, 1, 'ABRIAQUI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (15, 1, 'ALEJANDRIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (16, 1, 'AMAGA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (17, 1, 'AMALFI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (18, 1, 'ANDES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (19, 1, 'ANGELOPOLIS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (20, 1, 'ANGOSTURA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (21, 1, 'ANORI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (22, 1, 'SANTAFE DE ANTIOQUIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (23, 1, 'ANZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (24, 1, 'APARTADO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (25, 1, 'ARBOLETES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (26, 1, 'ARGELIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (27, 1, 'ARMENIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (28, 1, 'BARBOSA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (29, 1, 'BELMIRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (30, 1, 'BELLO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (31, 1, 'BETANIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (32, 1, 'BETULIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (33, 1, 'CIUDAD BOLIVAR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (34, 1, 'BRICEÑO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (35, 1, 'BURITICA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (36, 1, 'CACERES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (37, 1, 'CAICEDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (38, 1, 'CALDAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (39, 1, 'CAMPAMENTO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (40, 1, 'CAÑASGORDAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (41, 1, 'CARACOLI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (42, 1, 'CARAMANTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (43, 1, 'CAREPA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (44, 1, 'EL CARMEN DE VIBORAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (45, 1, 'CAROLINA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (46, 1, 'CAUCASIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (47, 1, 'CHIGORODO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (48, 1, 'CISNEROS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (49, 1, 'COCORNA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (50, 1, 'CONCEPCION');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (51, 1, 'CONCORDIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (52, 1, 'COPACABANA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (53, 1, 'DABEIBA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (54, 1, 'DON MATIAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (55, 1, 'EBEJICO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (56, 1, 'EL BAGRE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (57, 1, 'ENTRERRIOS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (58, 1, 'ENVIGADO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (59, 1, 'FREDONIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (60, 1, 'FRONTINO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (61, 1, 'GIRALDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (62, 1, 'GIRARDOTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (63, 1, 'GOMEZ PLATA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (64, 1, 'GRANADA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (65, 1, 'GUADALUPE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (66, 1, 'GUARNE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (67, 1, 'GUATAPE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (68, 1, 'HELICONIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (69, 1, 'HISPANIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (70, 1, 'ITAGUI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (71, 1, 'ITUANGO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (72, 1, 'JARDIN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (73, 1, 'JERICO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (74, 1, 'LA CEJA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (75, 1, 'LA ESTRELLA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (76, 1, 'LA PINTADA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (77, 1, 'LA UNION');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (78, 1, 'LIBORINA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (79, 1, 'MACEO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (80, 1, 'MARINILLA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (81, 1, 'MONTEBELLO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (82, 1, 'MURINDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (83, 1, 'MUTATA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (84, 1, 'NARIÑO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (85, 1, 'NECOCLI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (86, 1, 'NECHI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (87, 1, 'OLAYA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (88, 1, 'PEÑOL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (89, 1, 'PEQUE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (90, 1, 'PUEBLORRICO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (91, 1, 'PUERTO BERRIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (92, 1, 'PUERTO NARE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (93, 1, 'PUERTO TRIUNFO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (94, 1, 'REMEDIOS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (95, 1, 'RETIRO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (96, 1, 'RIONEGRO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (97, 1, 'SABANALARGA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (98, 1, 'SABANETA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (99, 1, 'SALGAR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (100, 1, 'SAN ANDRES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (101, 1, 'SAN CARLOS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (102, 1, 'SAN FRANCISCO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (103, 1, 'SAN JERONIMO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (104, 1, 'SAN JOSE DE LA MONTAÑA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (105, 1, 'SAN JUAN DE URABA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (106, 1, 'SAN LUIS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (107, 1, 'SAN PEDRO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (108, 1, 'SAN PEDRO DE URABA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (109, 1, 'SAN RAFAEL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (110, 1, 'SAN ROQUE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (111, 1, 'SAN VICENTE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (112, 1, 'SANTA BARBARA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (113, 1, 'SANTA ROSA DE OSOS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (114, 1, 'SANTO DOMINGO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (115, 1, 'EL SANTUARIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (116, 1, 'SEGOVIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (117, 1, 'SONSON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (118, 1, 'SOPETRAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (119, 1, 'TAMESIS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (120, 1, 'TARAZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (121, 1, 'TARSO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (122, 1, 'TITIRIBI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (123, 1, 'TOLEDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (124, 1, 'TURBO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (125, 1, 'URAMITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (126, 1, 'URRAO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (127, 1, 'VALDIVIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (128, 1, 'VALPARAISO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (129, 1, 'VEGACHI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (130, 1, 'VENECIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (131, 1, 'VIGIA DEL FUERTE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (132, 1, 'YALI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (133, 1, 'YARUMAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (134, 1, 'YOLOMBO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (135, 1, 'YONDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (136, 1, 'ZARAGOZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (137, 25, 'ARAUCA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (138, 25, 'ARAUQUITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (139, 25, 'CRAVO NORTE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (140, 25, 'FORTUL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (141, 25, 'PUERTO RONDON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (142, 25, 'SARAVENA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (143, 25, 'TAME');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (144, 28, 'SAN ANDRES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (145, 28, 'PROVIDENCIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (146, 2, 'BARRANQUILLA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (147, 2, 'BARANOA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (148, 2, 'CAMPO DE LA CRUZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (149, 2, 'CANDELARIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (150, 2, 'GALAPA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (151, 2, 'JUAN DE ACOSTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (152, 2, 'LURUACO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (153, 2, 'MALAMBO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (154, 2, 'MANATI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (155, 2, 'PALMAR DE VARELA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (156, 2, 'PIOJO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (157, 2, 'POLONUEVO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (158, 2, 'PONEDERA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (159, 2, 'PUERTO COLOMBIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (160, 2, 'REPELON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (161, 2, 'SABANAGRANDE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (162, 2, 'SABANALARGA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (163, 2, 'SANTA LUCIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (164, 2, 'SANTO TOMAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (165, 2, 'SOLEDAD');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (166, 2, 'SUAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (167, 2, 'TUBARA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (168, 2, 'USIACURI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (169, 3, 'BOGOTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (170, 4, 'CARTAGENA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (171, 4, 'ACHI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (172, 4, 'ALTOS DEL ROSARIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (173, 4, 'ARENAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (174, 4, 'ARJONA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (175, 4, 'ARROYOHONDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (176, 4, 'BARRANCO DE LOBA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (177, 4, 'CALAMAR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (178, 4, 'CANTAGALLO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (179, 4, 'CICUCO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (180, 4, 'CORDOBA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (181, 4, 'CLEMENCIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (182, 4, 'EL CARMEN DE BOLIVAR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (183, 4, 'EL GUAMO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (184, 4, 'EL PEÑON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (185, 4, 'HATILLO DE LOBA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (186, 4, 'MAGANGUE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (187, 4, 'MAHATES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (188, 4, 'MARGARITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (189, 4, 'MARIA LA BAJA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (190, 4, 'MONTECRISTO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (191, 4, 'MOMPOS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (192, 4, 'MORALES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (193, 4, 'PINILLOS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (194, 4, 'REGIDOR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (195, 4, 'RIO VIEJO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (196, 4, 'SAN CRISTOBAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (197, 4, 'SAN ESTANISLAO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (198, 4, 'SAN FERNANDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (199, 4, 'SAN JACINTO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (200, 4, 'SAN JACINTO DEL CAUCA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (201, 4, 'SAN JUAN NEPOMUCENO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (202, 4, 'SAN MARTIN DE LOBA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (203, 4, 'SAN PABLO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (204, 4, 'SANTA CATALINA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (205, 4, 'SANTA ROSA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (206, 4, 'SANTA ROSA DEL SUR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (207, 4, 'SIMITI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (208, 4, 'SOPLAVIENTO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (209, 4, 'TALAIGUA NUEVO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (210, 4, 'TIQUISIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (211, 4, 'TURBACO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (212, 4, 'TURBANA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (213, 4, 'VILLANUEVA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (214, 4, 'ZAMBRANO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (215, 5, 'TUNJA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (216, 5, 'ALMEIDA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (217, 5, 'AQUITANIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (218, 5, 'ARCABUCO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (219, 5, 'BELEN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (220, 5, 'BERBEO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (221, 5, 'BETEITIVA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (222, 5, 'BOAVITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (223, 5, 'BOYACA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (224, 5, 'BRICEÑO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (225, 5, 'BUENAVISTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (226, 5, 'BUSBANZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (227, 5, 'CALDAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (228, 5, 'CAMPOHERMOSO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (229, 5, 'CERINZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (230, 5, 'CHINAVITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (231, 5, 'CHIQUINQUIRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (232, 5, 'CHISCAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (233, 5, 'CHITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (234, 5, 'CHITARAQUE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (235, 5, 'CHIVATA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (236, 5, 'CIENEGA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (237, 5, 'COMBITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (238, 5, 'COPER');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (239, 5, 'CORRALES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (240, 5, 'COVARACHIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (241, 5, 'CUBARA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (242, 5, 'CUCAITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (243, 5, 'CUITIVA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (244, 5, 'CHIQUIZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (245, 5, 'CHIVOR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (246, 5, 'DUITAMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (247, 5, 'EL COCUY');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (248, 5, 'EL ESPINO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (249, 5, 'FIRAVITOBA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (250, 5, 'FLORESTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (251, 5, 'GACHANTIVA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (252, 5, 'GAMEZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (253, 5, 'GARAGOA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (254, 5, 'GUACAMAYAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (255, 5, 'GUATEQUE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (256, 5, 'GUAYATA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (257, 5, 'GÜICAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (258, 5, 'IZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (259, 5, 'JENESANO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (260, 5, 'JERICO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (261, 5, 'LABRANZAGRANDE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (262, 5, 'LA CAPILLA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (263, 5, 'LA VICTORIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (264, 5, 'LA UVITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (265, 5, 'VILLA DE LEYVA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (266, 5, 'MACANAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (267, 5, 'MARIPI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (268, 5, 'MIRAFLORES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (269, 5, 'MONGUA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (270, 5, 'MONGUI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (271, 5, 'MONIQUIRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (272, 5, 'MOTAVITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (273, 5, 'MUZO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (274, 5, 'NOBSA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (275, 5, 'NUEVO COLON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (276, 5, 'OICATA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (277, 5, 'OTANCHE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (278, 5, 'PACHAVITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (279, 5, 'PAEZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (280, 5, 'PAIPA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (281, 5, 'PAJARITO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (282, 5, 'PANQUEBA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (283, 5, 'PAUNA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (284, 5, 'PAYA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (285, 5, 'PAZ DE RIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (286, 5, 'PESCA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (287, 5, 'PISBA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (288, 5, 'PUERTO BOYACA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (289, 5, 'QUIPAMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (290, 5, 'RAMIRIQUI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (291, 5, 'RAQUIRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (292, 5, 'RONDON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (293, 5, 'SABOYA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (294, 5, 'SACHICA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (295, 5, 'SAMACA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (296, 5, 'SAN EDUARDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (297, 5, 'SAN JOSE DE PARE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (298, 5, 'SAN LUIS DE GACENO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (299, 5, 'SAN MATEO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (300, 5, 'SAN MIGUEL DE SEMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (301, 5, 'SAN PABLO DE BORBUR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (302, 5, 'SANTANA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (303, 5, 'SANTA MARIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (304, 5, 'SANTA ROSA DE VITERBO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (305, 5, 'SANTA SOFIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (306, 5, 'SATIVANORTE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (307, 5, 'SATIVASUR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (308, 5, 'SIACHOQUE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (309, 5, 'SOATA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (310, 5, 'SOCOTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (311, 5, 'SOCHA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (312, 5, 'SOGAMOSO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (313, 5, 'SOMONDOCO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (314, 5, 'SORA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (315, 5, 'SOTAQUIRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (316, 5, 'SORACA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (317, 5, 'SUSACON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (318, 5, 'SUTAMARCHAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (319, 5, 'SUTATENZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (320, 5, 'TASCO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (321, 5, 'TENZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (322, 5, 'TIBANA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (323, 5, 'TIBASOSA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (324, 5, 'TINJACA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (325, 5, 'TIPACOQUE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (326, 5, 'TOCA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (327, 5, 'TOGÜI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (328, 5, 'TOPAGA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (329, 5, 'TOTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (330, 5, 'TUNUNGUA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (331, 5, 'TURMEQUE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (332, 5, 'TUTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (333, 5, 'TUTAZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (334, 5, 'UMBITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (335, 5, 'VENTAQUEMADA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (336, 5, 'VIRACACHA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (337, 5, 'ZETAQUIRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (338, 6, 'MANIZALES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (339, 6, 'AGUADAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (340, 6, 'ANSERMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (341, 6, 'ARANZAZU');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (342, 6, 'BELALCAZAR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (343, 6, 'CHINCHINA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (344, 6, 'FILADELFIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (345, 6, 'LA DORADA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (346, 6, 'LA MERCED');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (347, 6, 'MANZANARES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (348, 6, 'MARMATO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (349, 6, 'MARQUETALIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (350, 6, 'MARULANDA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (351, 6, 'NEIRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (352, 6, 'NORCASIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (353, 6, 'PACORA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (354, 6, 'PALESTINA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (355, 6, 'PENSILVANIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (356, 6, 'RIOSUCIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (357, 6, 'RISARALDA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (358, 6, 'SALAMINA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (359, 6, 'SAMANA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (360, 6, 'SAN JOSE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (361, 6, 'SUPIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (362, 6, 'VICTORIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (363, 6, 'VILLAMARIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (364, 6, 'VITERBO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (365, 7, 'FLORENCIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (366, 7, 'ALBANIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (367, 7, 'BELEN DE LOS ANDAQUIES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (368, 7, 'CARTAGENA DEL CHAIRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (369, 7, 'CURILLO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (370, 7, 'EL DONCELLO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (371, 7, 'EL PAUJIL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (372, 7, 'LA MONTAÑITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (373, 7, 'MILAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (374, 7, 'MORELIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (375, 7, 'PUERTO RICO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (376, 7, 'SAN JOSE DEL FRAGUA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (377, 7, 'SAN VICENTE DEL CAGUAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (378, 7, 'SOLANO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (379, 7, 'SOLITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (380, 7, 'VALPARAISO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (381, 26, 'YOPAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (382, 26, 'AGUAZUL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (383, 26, 'CHAMEZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (384, 26, 'HATO COROZAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (385, 26, 'LA SALINA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (386, 26, 'MANI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (387, 26, 'MONTERREY');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (388, 26, 'NUNCHIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (389, 26, 'OROCUE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (390, 26, 'PAZ DE ARIPORO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (391, 26, 'PORE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (392, 26, 'RECETOR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (393, 26, 'SABANALARGA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (394, 26, 'SACAMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (395, 26, 'SAN LUIS DE PALENQUE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (396, 26, 'TAMARA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (397, 26, 'TAURAMENA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (398, 26, 'TRINIDAD');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (399, 26, 'VILLANUEVA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (400, 8, 'POPAYAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (401, 8, 'ALMAGUER');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (402, 8, 'ARGELIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (403, 8, 'BALBOA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (404, 8, 'BOLIVAR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (405, 8, 'BUENOS AIRES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (406, 8, 'CAJIBIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (407, 8, 'CALDONO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (408, 8, 'CALOTO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (409, 8, 'CORINTO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (410, 8, 'EL TAMBO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (411, 8, 'FLORENCIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (412, 8, 'GUAPI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (413, 8, 'INZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (414, 8, 'JAMBALO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (415, 8, 'LA SIERRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (416, 8, 'LA VEGA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (417, 8, 'LOPEZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (418, 8, 'MERCADERES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (419, 8, 'MIRANDA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (420, 8, 'MORALES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (421, 8, 'PADILLA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (422, 8, 'PAEZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (423, 8, 'PATIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (424, 8, 'PIAMONTE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (425, 8, 'PIENDAMO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (426, 8, 'PUERTO TEJADA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (427, 8, 'PURACE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (428, 8, 'ROSAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (429, 8, 'SAN SEBASTIAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (430, 8, 'SANTANDER DE QUILICHAO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (431, 8, 'SANTA ROSA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (432, 8, 'SILVIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (433, 8, 'SOTARA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (434, 8, 'SUAREZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (435, 8, 'SUCRE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (436, 8, 'TIMBIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (437, 8, 'TIMBIQUI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (438, 8, 'TORIBIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (439, 8, 'TOTORO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (440, 8, 'VILLA RICA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (441, 9, 'VALLEDUPAR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (442, 9, 'AGUACHICA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (443, 9, 'AGUSTIN CODAZZI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (444, 9, 'ASTREA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (445, 9, 'BECERRIL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (446, 9, 'BOSCONIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (447, 9, 'CHIMICHAGUA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (448, 9, 'CHIRIGUANA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (449, 9, 'CURUMANI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (450, 9, 'EL COPEY');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (451, 9, 'EL PASO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (452, 9, 'GAMARRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (453, 9, 'GONZALEZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (454, 9, 'LA GLORIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (455, 9, 'LA JAGUA DE IBIRICO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (456, 9, 'MANAURE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (457, 9, 'PAILITAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (458, 9, 'PELAYA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (459, 9, 'PUEBLO BELLO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (460, 9, 'RIO DE ORO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (461, 9, 'LA PAZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (462, 9, 'SAN ALBERTO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (463, 9, 'SAN DIEGO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (464, 9, 'SAN MARTIN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (465, 9, 'TAMALAMEQUE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (466, 12, 'QUIBDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (467, 12, 'ACANDI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (468, 12, 'ALTO BAUDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (469, 12, 'ATRATO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (470, 12, 'BAGADO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (471, 12, 'BAHIA SOLANO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (472, 12, 'BAJO BAUDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (473, 12, 'BELEN DE BAJIRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (474, 12, 'BOJAYA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (475, 12, 'EL CANTON DEL SAN PABLO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (476, 12, 'CARMEN DEL DARIEN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (477, 12, 'CERTEGUI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (478, 12, 'CONDOTO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (479, 12, 'EL CARMEN DE ATRATO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (480, 12, 'EL LITORAL DEL SAN JUAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (481, 12, 'ISTMINA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (482, 12, 'JURADO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (483, 12, 'LLORO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (484, 12, 'MEDIO ATRATO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (485, 12, 'MEDIO BAUDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (486, 12, 'MEDIO SAN JUAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (487, 12, 'NOVITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (488, 12, 'NUQUI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (489, 12, 'RIO IRO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (490, 12, 'RIO QUITO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (491, 12, 'RIOSUCIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (492, 12, 'SAN JOSE DEL PALMAR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (493, 12, 'SIPI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (494, 12, 'TADO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (495, 12, 'UNGUIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (496, 12, 'UNION PANAMERICANA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (497, 10, 'MONTERIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (498, 10, 'AYAPEL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (499, 10, 'BUENAVISTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (500, 10, 'CANALETE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (501, 10, 'CERETE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (502, 10, 'CHIMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (503, 10, 'CHINU');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (504, 10, 'CIENAGA DE ORO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (505, 10, 'COTORRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (506, 10, 'LA APARTADA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (507, 10, 'LORICA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (508, 10, 'LOS CORDOBAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (509, 10, 'MOMIL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (510, 10, 'MONTELIBANO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (511, 10, 'MOÑITOS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (512, 10, 'PLANETA RICA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (513, 10, 'PUEBLO NUEVO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (514, 10, 'PUERTO ESCONDIDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (515, 10, 'PUERTO LIBERTADOR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (516, 10, 'PURISIMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (517, 10, 'SAHAG/N');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (518, 10, 'SAN ANDRES SOTAVENTO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (519, 10, 'SAN ANTERO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (520, 10, 'SAN BERNARDO DEL VIENTO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (521, 10, 'SAN CARLOS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (522, 10, 'SAN PELAYO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (523, 10, 'TIERRALTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (524, 10, 'VALENCIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (525, 11, 'AGUA DE DIOS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (526, 11, 'ALBAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (527, 11, 'ANAPOIMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (528, 11, 'ANOLAIMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (529, 11, 'ARBELAEZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (530, 11, 'BELTRAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (531, 11, 'BITUIMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (532, 11, 'BOJACA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (533, 11, 'CABRERA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (534, 11, 'CACHIPAY');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (535, 11, 'CAJICA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (536, 11, 'CAPARRAPI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (537, 11, 'CAQUEZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (538, 11, 'CARMEN DE CARUPA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (539, 11, 'CHAGUANI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (540, 11, 'CHIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (541, 11, 'CHIPAQUE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (542, 11, 'CHOACHI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (543, 11, 'CHOCONTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (544, 11, 'COGUA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (545, 11, 'COTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (546, 11, 'CUCUNUBA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (547, 11, 'EL COLEGIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (548, 11, 'EL PEÑON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (549, 11, 'EL ROSAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (550, 11, 'FACATATIVA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (551, 11, 'FOMEQUE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (552, 11, 'FOSCA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (553, 11, 'FUNZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (554, 11, 'F/QUENE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (555, 11, 'FUSAGASUGA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (556, 11, 'GACHALA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (557, 11, 'GACHANCIPA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (558, 11, 'GACHETA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (559, 11, 'GAMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (560, 11, 'GIRARDOT');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (561, 11, 'GRANADA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (562, 11, 'GUACHETA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (563, 11, 'GUADUAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (564, 11, 'GUASCA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (565, 11, 'GUATAQUI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (566, 11, 'GUATAVITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (567, 11, 'GUAYABAL DE SIQUIMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (568, 11, 'GUAYABETAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (569, 11, 'GUTIERREZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (570, 11, 'JERUSALEN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (571, 11, 'JUNIN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (572, 11, 'LA CALERA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (573, 11, 'LA MESA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (574, 11, 'LA PALMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (575, 11, 'LA PEÑA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (576, 11, 'LA VEGA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (577, 11, 'LENGUAZAQUE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (578, 11, 'MACHETA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (579, 11, 'MADRID');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (580, 11, 'MANTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (581, 11, 'MEDINA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (582, 11, 'MOSQUERA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (583, 11, 'NARIÑO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (584, 11, 'NEMOCON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (585, 11, 'NILO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (586, 11, 'NIMAIMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (587, 11, 'NOCAIMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (588, 11, 'VENECIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (589, 11, 'PACHO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (590, 11, 'PAIME');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (591, 11, 'PANDI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (592, 11, 'PARATEBUENO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (593, 11, 'PASCA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (594, 11, 'PUERTO SALGAR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (595, 11, 'PULI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (596, 11, 'QUEBRADANEGRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (597, 11, 'QUETAME');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (598, 11, 'QUIPILE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (599, 11, 'APULO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (600, 11, 'RICAURTE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (601, 11, 'SAN ANTONIO DEL TEQUENDAMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (602, 11, 'SAN BERNARDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (603, 11, 'SAN CAYETANO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (604, 11, 'SAN FRANCISCO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (605, 11, 'SAN JUAN DE RIO SECO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (606, 11, 'SASAIMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (607, 11, 'SESQUILE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (608, 11, 'SIBATE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (609, 11, 'SILVANIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (610, 11, 'SIMIJACA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (611, 11, 'SOACHA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (612, 11, 'SOPO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (613, 11, 'SUBACHOQUE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (614, 11, 'SUESCA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (615, 11, 'SUPATA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (616, 11, 'SUSA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (617, 11, 'SUTATAUSA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (618, 11, 'TABIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (619, 11, 'TAUSA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (620, 11, 'TENA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (621, 11, 'TENJO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (622, 11, 'TIBACUY');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (623, 11, 'TIBIRITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (624, 11, 'TOCAIMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (625, 11, 'TOCANCIPA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (626, 11, 'TOPAIPI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (627, 11, 'UBALA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (628, 11, 'UBAQUE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (629, 11, 'VILLA DE SAN DIEGO DE UBATE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (630, 11, 'UNE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (631, 11, 'UTICA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (632, 11, 'VERGARA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (633, 11, 'VIANI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (634, 11, 'VILLAGOMEZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (635, 11, 'VILLAPINZON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (636, 11, 'VILLETA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (637, 11, 'VIOTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (638, 11, 'YACOPI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (639, 11, 'ZIPACON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (640, 11, 'ZIPAQUIRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (641, 30, 'INIRIDA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (642, 30, 'BARRANCO MINAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (643, 30, 'MAPIRIPANA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (644, 30, 'SAN FELIPE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (645, 30, 'PUERTO COLOMBIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (646, 30, 'LA GUADALUPE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (647, 30, 'CACAHUAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (648, 30, 'PANA PANA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (649, 30, 'MORICHAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (650, 31, 'SAN JOSE DEL GUAVIARE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (651, 31, 'CALAMAR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (652, 31, 'EL RETORNO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (653, 31, 'MIRAFLORES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (654, 13, 'NEIVA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (655, 13, 'ACEVEDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (656, 13, 'AGRADO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (657, 13, 'AIPE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (658, 13, 'ALGECIRAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (659, 13, 'ALTAMIRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (660, 13, 'BARAYA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (661, 13, 'CAMPOALEGRE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (662, 13, 'COLOMBIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (663, 13, 'ELIAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (664, 13, 'GARZON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (665, 13, 'GIGANTE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (666, 13, 'GUADALUPE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (667, 13, 'HOBO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (668, 13, 'IQUIRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (669, 13, 'ISNOS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (670, 13, 'LA ARGENTINA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (671, 13, 'LA PLATA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (672, 13, 'NATAGA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (673, 13, 'OPORAPA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (674, 13, 'PAICOL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (675, 13, 'PALERMO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (676, 13, 'PALESTINA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (677, 13, 'PITAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (678, 13, 'PITALITO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (679, 13, 'RIVERA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (680, 13, 'SALADOBLANCO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (681, 13, 'SAN AGUSTIN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (682, 13, 'SANTA MARIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (683, 13, 'SUAZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (684, 13, 'TARQUI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (685, 13, 'TESALIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (686, 13, 'TELLO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (687, 13, 'TERUEL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (688, 13, 'TIMANA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (689, 13, 'VILLAVIEJA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (690, 13, 'YAGUARA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (691, 14, 'RIOHACHA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (692, 14, 'ALBANIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (693, 14, 'BARRANCAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (694, 14, 'DIBULLA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (695, 14, 'DISTRACCION');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (696, 14, 'EL MOLINO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (697, 14, 'FONSECA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (698, 14, 'HATONUEVO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (699, 14, 'LA JAGUA DEL PILAR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (700, 14, 'MAICAO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (701, 14, 'MANAURE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (702, 14, 'SAN JUAN DEL CESAR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (703, 14, 'URIBIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (704, 14, 'URUMITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (705, 14, 'VILLANUEVA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (706, 15, 'SANTA MARTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (707, 15, 'ALGARROBO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (708, 15, 'ARACATACA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (709, 15, 'ARIGUANI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (710, 15, 'CERRO SAN ANTONIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (711, 15, 'CHIBOLO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (712, 15, 'CIENAGA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (713, 15, 'CONCORDIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (714, 15, 'EL BANCO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (715, 15, 'EL PIÑON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (716, 15, 'EL RETEN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (717, 15, 'FUNDACION');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (718, 15, 'GUAMAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (719, 15, 'NUEVA GRANADA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (720, 15, 'PEDRAZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (721, 15, 'PIJIÑO DEL CARMEN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (722, 15, 'PIVIJAY');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (723, 15, 'PLATO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (724, 15, 'PUEBLOVIEJO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (725, 15, 'REMOLINO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (726, 15, 'SABANAS DE SAN ANGEL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (727, 15, 'SALAMINA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (728, 15, 'SAN SEBASTIAN DE BUENAVISTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (729, 15, 'SAN ZENON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (730, 15, 'SANTA ANA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (731, 15, 'SANTA BARBARA DE PINTO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (732, 15, 'SITIONUEVO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (733, 15, 'TENERIFE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (734, 15, 'ZAPAYAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (735, 15, 'ZONA BANANERA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (736, 16, 'VILLAVICENCIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (737, 16, 'ACACIAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (738, 16, 'BARRANCA DE UPIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (739, 16, 'CABUYARO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (740, 16, 'CASTILLA LA NUEVA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (741, 16, 'CUBARRAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (742, 16, 'CUMARAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (743, 16, 'EL CALVARIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (744, 16, 'EL CASTILLO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (745, 16, 'EL DORADO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (746, 16, 'FUENTE DE ORO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (747, 16, 'GRANADA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (748, 16, 'GUAMAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (749, 16, 'MAPIRIPAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (750, 16, 'MESETAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (751, 16, 'LA MACARENA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (752, 16, 'URIBE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (753, 16, 'LEJANIAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (754, 16, 'PUERTO CONCORDIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (755, 16, 'PUERTO GAITAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (756, 16, 'PUERTO LOPEZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (757, 16, 'PUERTO LLERAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (758, 16, 'PUERTO RICO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (759, 16, 'RESTREPO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (760, 16, 'SAN CARLOS DE GUAROA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (761, 16, 'SAN JUAN DE ARAMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (762, 16, 'SAN JUANITO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (763, 16, 'SAN MARTIN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (764, 16, 'VISTAHERMOSA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (765, 17, 'PASTO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (766, 17, 'ALBAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (767, 17, 'ALDANA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (768, 17, 'ANCUYA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (769, 17, 'ARBOLEDA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (770, 17, 'BARBACOAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (771, 17, 'BELEN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (772, 17, 'BUESACO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (773, 17, 'COLON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (774, 17, 'CONSACA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (775, 17, 'CONTADERO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (776, 17, 'CORDOBA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (777, 17, 'CUASPUD');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (778, 17, 'CUMBAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (779, 17, 'CUMBITARA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (780, 17, 'CHACHAGÜI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (781, 17, 'EL CHARCO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (782, 17, 'EL PEÑOL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (783, 17, 'EL ROSARIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (784, 17, 'EL TABLON DE GOMEZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (785, 17, 'EL TAMBO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (786, 17, 'FUNES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (787, 17, 'GUACHUCAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (788, 17, 'GUAITARILLA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (789, 17, 'GUALMATAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (790, 17, 'ILES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (791, 17, 'IMUES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (792, 17, 'IPIALES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (793, 17, 'LA CRUZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (794, 17, 'LA FLORIDA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (795, 17, 'LA LLANADA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (796, 17, 'LA TOLA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (797, 17, 'LA UNION');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (798, 17, 'LEIVA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (799, 17, 'LINARES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (800, 17, 'LOS ANDES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (801, 17, 'MAGÜI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (802, 17, 'MALLAMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (803, 17, 'MOSQUERA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (804, 17, 'NARIÑO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (805, 17, 'OLAYA HERRERA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (806, 17, 'OSPINA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (807, 17, 'FRANCISCO PIZARRO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (808, 17, 'POLICARPA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (809, 17, 'POTOSI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (810, 17, 'PROVIDENCIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (811, 17, 'PUERRES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (812, 17, 'PUPIALES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (813, 17, 'RICAURTE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (814, 17, 'ROBERTO PAYAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (815, 17, 'SAMANIEGO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (816, 17, 'SANDONA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (817, 17, 'SAN BERNARDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (818, 17, 'SAN LORENZO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (819, 17, 'SAN PABLO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (820, 17, 'SAN PEDRO DE CARTAGO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (821, 17, 'SANTA BARBARA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (822, 17, 'SANTACRUZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (823, 17, 'SAPUYES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (824, 17, 'TAMINANGO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (825, 17, 'TANGUA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (826, 17, 'TUMACO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (827, 17, 'TUQUERRES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (828, 17, 'YACUANQUER');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (829, 18, 'C/CUTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (830, 18, 'ABREGO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (831, 18, 'ARBOLEDAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (832, 18, 'BOCHALEMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (833, 18, 'BUCARASICA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (834, 18, 'CACOTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (835, 18, 'CACHIRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (836, 18, 'CHINACOTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (837, 18, 'CHITAGA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (838, 18, 'CONVENCION');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (839, 18, 'CUCUTILLA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (840, 18, 'DURANIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (841, 18, 'EL CARMEN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (842, 18, 'EL TARRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (843, 18, 'EL ZULIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (844, 18, 'GRAMALOTE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (845, 18, 'HACARI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (846, 18, 'HERRAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (847, 18, 'LABATECA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (848, 18, 'LA ESPERANZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (849, 18, 'LA PLAYA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (850, 18, 'LOS PATIOS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (851, 18, 'LOURDES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (852, 18, 'MUTISCUA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (853, 18, 'OCAÑA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (854, 18, 'PAMPLONA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (855, 18, 'PAMPLONITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (856, 18, 'PUERTO SANTANDER');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (857, 18, 'RAGONVALIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (858, 18, 'SALAZAR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (859, 18, 'SAN CALIXTO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (860, 18, 'SAN CAYETANO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (861, 18, 'SANTIAGO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (862, 18, 'SARDINATA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (863, 18, 'SILOS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (864, 18, 'TEORAMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (865, 18, 'TIBU');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (866, 18, 'TOLEDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (867, 18, 'VILLA CARO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (868, 18, 'VILLA DEL ROSARIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (869, 27, 'MOCOA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (870, 27, 'COLON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (871, 27, 'ORITO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (872, 27, 'PUERTO ASIS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (873, 27, 'PUERTO CAICEDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (874, 27, 'PUERTO GUZMAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (875, 27, 'LEGUIZAMO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (876, 27, 'SIBUNDOY');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (877, 27, 'SAN FRANCISCO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (878, 27, 'SAN MIGUEL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (879, 27, 'SANTIAGO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (880, 27, 'VALLE DEL GUAMUEZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (881, 27, 'VILLAGARZON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (882, 19, 'ARMENIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (883, 19, 'BUENAVISTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (884, 19, 'CALARCA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (885, 19, 'CIRCASIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (886, 19, 'CORDOBA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (887, 19, 'FILANDIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (888, 19, 'GENOVA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (889, 19, 'LA TEBAIDA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (890, 19, 'MONTENEGRO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (891, 19, 'PIJAO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (892, 19, 'QUIMBAYA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (893, 19, 'SALENTO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (894, 20, 'PEREIRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (895, 20, 'APIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (896, 20, 'BALBOA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (897, 20, 'BELEN DE UMBRIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (898, 20, 'DOSQUEBRADAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (899, 20, 'GUATICA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (900, 20, 'LA CELIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (901, 20, 'LA VIRGINIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (902, 20, 'MARSELLA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (903, 20, 'MISTRATO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (904, 20, 'PUEBLO RICO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (905, 20, 'QUINCHIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (906, 20, 'SANTA ROSA DE CABAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (907, 20, 'SANTUARIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (908, 21, 'BUCARAMANGA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (909, 21, 'AGUADA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (910, 21, 'ALBANIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (911, 21, 'ARATOCA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (912, 21, 'BARBOSA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (913, 21, 'BARICHARA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (914, 21, 'BARRANCABERMEJA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (915, 21, 'BETULIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (916, 21, 'BOLIVAR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (917, 21, 'CABRERA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (918, 21, 'CALIFORNIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (919, 21, 'CAPITANEJO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (920, 21, 'CARCASI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (921, 21, 'CEPITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (922, 21, 'CERRITO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (923, 21, 'CHARALA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (924, 21, 'CHARTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (925, 21, 'CHIMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (926, 21, 'CHIPATA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (927, 21, 'CIMITARRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (928, 21, 'CONCEPCION');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (929, 21, 'CONFINES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (930, 21, 'CONTRATACION');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (931, 21, 'COROMORO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (932, 21, 'CURITI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (933, 21, 'EL CARMEN DE CHUCURI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (934, 21, 'EL GUACAMAYO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (935, 21, 'EL PEÑON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (936, 21, 'EL PLAYON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (937, 21, 'ENCINO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (938, 21, 'ENCISO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (939, 21, 'FLORIAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (940, 21, 'FLORIDABLANCA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (941, 21, 'GALAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (942, 21, 'GAMBITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (943, 21, 'GIRON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (944, 21, 'GUACA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (945, 21, 'GUADALUPE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (946, 21, 'GUAPOTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (947, 21, 'GUAVATA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (948, 21, 'GÜEPSA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (949, 21, 'HATO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (950, 21, 'JES/S MARIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (951, 21, 'JORDAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (952, 21, 'LA BELLEZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (953, 21, 'LANDAZURI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (954, 21, 'LA PAZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (955, 21, 'LEBRIJA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (956, 21, 'LOS SANTOS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (957, 21, 'MACARAVITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (958, 21, 'MALAGA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (959, 21, 'MATANZA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (960, 21, 'MOGOTES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (961, 21, 'MOLAGAVITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (962, 21, 'OCAMONTE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (963, 21, 'OIBA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (964, 21, 'ONZAGA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (965, 21, 'PALMAR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (966, 21, 'PALMAS DEL SOCORRO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (967, 21, 'PARAMO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (968, 21, 'PIEDECUESTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (969, 21, 'PINCHOTE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (970, 21, 'PUENTE NACIONAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (971, 21, 'PUERTO PARRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (972, 21, 'PUERTO WILCHES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (973, 21, 'RIONEGRO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (974, 21, 'SABANA DE TORRES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (975, 21, 'SAN ANDRES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (976, 21, 'SAN BENITO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (977, 21, 'SAN GIL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (978, 21, 'SAN JOAQUIN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (979, 21, 'SAN JOSE DE MIRANDA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (980, 21, 'SAN MIGUEL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (981, 21, 'SAN VICENTE DE CHUCURI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (982, 21, 'SANTA BARBARA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (983, 21, 'SANTA HELENA DEL OPON');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (984, 21, 'SIMACOTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (985, 21, 'SOCORRO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (986, 21, 'SUAITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (987, 21, 'SUCRE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (988, 21, 'SURATA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (989, 21, 'TONA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (990, 21, 'VALLE DE SAN JOSE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (991, 21, 'VELEZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (992, 21, 'VETAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (993, 21, 'VILLANUEVA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (994, 21, 'ZAPATOCA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (995, 22, 'SINCELEJO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (996, 22, 'BUENAVISTA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (997, 22, 'CAIMITO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (998, 22, 'COLOSO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (999, 22, 'COROZAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1000, 22, 'COVEÑAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1001, 22, 'CHALAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1002, 22, 'EL ROBLE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1003, 22, 'GALERAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1004, 22, 'GUARANDA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1005, 22, 'LA UNION');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1006, 22, 'LOS PALMITOS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1007, 22, 'MAJAGUAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1008, 22, 'MORROA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1009, 22, 'OVEJAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1010, 22, 'PALMITO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1011, 22, 'SAMPUES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1012, 22, 'SAN BENITO ABAD');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1013, 22, 'SAN JUAN DE BETULIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1014, 22, 'SAN MARCOS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1015, 22, 'SAN ONOFRE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1016, 22, 'SAN PEDRO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1017, 22, 'SINCE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1018, 22, 'SUCRE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1019, 22, 'SANTIAGO DE TOLU');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1020, 22, 'TOLUVIEJO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1021, 23, 'IBAGUE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1022, 23, 'ALPUJARRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1023, 23, 'ALVARADO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1024, 23, 'AMBALEMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1025, 23, 'ANZOATEGUI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1026, 23, 'ARMERO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1027, 23, 'ATACO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1028, 23, 'CAJAMARCA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1029, 23, 'CARMEN DE APICALA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1030, 23, 'CASABIANCA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1031, 23, 'CHAPARRAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1032, 23, 'COELLO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1033, 23, 'COYAIMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1034, 23, 'CUNDAY');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1035, 23, 'DOLORES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1036, 23, 'ESPINAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1037, 23, 'FALAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1038, 23, 'FLANDES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1039, 23, 'FRESNO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1040, 23, 'GUAMO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1041, 23, 'HERVEO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1042, 23, 'HONDA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1043, 23, 'ICONONZO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1044, 23, 'LERIDA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1045, 23, 'LIBANO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1046, 23, 'MARIQUITA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1047, 23, 'MELGAR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1048, 23, 'MURILLO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1049, 23, 'NATAGAIMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1050, 23, 'ORTEGA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1051, 23, 'PALOCABILDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1052, 23, 'PIEDRAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1053, 23, 'PLANADAS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1054, 23, 'PRADO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1055, 23, 'PURIFICACION');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1056, 23, 'RIOBLANCO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1057, 23, 'RONCESVALLES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1058, 23, 'ROVIRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1059, 23, 'SALDAÑA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1060, 23, 'SAN ANTONIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1061, 23, 'SAN LUIS');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1062, 23, 'SANTA ISABEL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1063, 23, 'SUAREZ');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1064, 23, 'VALLE DE SAN JUAN');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1065, 23, 'VENADILLO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1066, 23, 'VILLAHERMOSA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1067, 23, 'VILLARRICA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1068, 24, 'CALI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1069, 24, 'ALCALA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1070, 24, 'ANDALUCIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1071, 24, 'ANSERMANUEVO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1072, 24, 'ARGELIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1073, 24, 'BOLIVAR');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1074, 24, 'BUENAVENTURA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1075, 24, 'GUADALAJARA DE BUGA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1076, 24, 'BUGALAGRANDE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1077, 24, 'CAICEDONIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1078, 24, 'CALIMA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1079, 24, 'CANDELARIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1080, 24, 'CARTAGO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1081, 24, 'DAGUA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1082, 24, 'EL AGUILA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1083, 24, 'EL CAIRO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1084, 24, 'EL CERRITO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1085, 24, 'EL DOVIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1086, 24, 'FLORIDA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1087, 24, 'GINEBRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1088, 24, 'GUACARI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1089, 24, 'JAMUNDI');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1090, 24, 'LA CUMBRE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1091, 24, 'LA UNION');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1092, 24, 'LA VICTORIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1093, 24, 'OBANDO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1094, 24, 'PALMIRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1095, 24, 'PRADERA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1096, 24, 'RESTREPO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1097, 24, 'RIOFRIO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1098, 24, 'ROLDANILLO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1099, 24, 'SAN PEDRO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1100, 24, 'SEVILLA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1101, 24, 'TORO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1102, 24, 'TRUJILLO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1103, 24, 'TULUA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1104, 24, 'ULLOA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1105, 24, 'VERSALLES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1106, 24, 'VIJES');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1107, 24, 'YOTOCO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1108, 24, 'YUMBO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1109, 24, 'ZARZAL');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1110, 32, 'MITU');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1111, 32, 'CARURU');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1112, 32, 'PACOA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1113, 32, 'TARAIRA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1114, 32, 'PAPUNAUA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1115, 32, 'YAVARATE');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1116, 33, 'PUERTO CARREÑO');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1117, 33, 'LA PRIMAVERA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1118, 33, 'SANTA ROSALIA');
INSERT INTO spen15_my_ciudad (id, id_depto, nombre) VALUES (1119, 33, 'CUMARIBO');

INSERT INTO spen15_menu (menutype, title, alias, note, path, link, type, published, parent_id, level, component_id, checked_out, checked_out_time, browserNav, access, img, template_style_id, params, lft, rgt, home, language, client_id) 
VALUES ('menu', 'com_my_catalogo', 'Mi catalogo', '', 'Catalogo y carrito de compras', 'index.php?option=com_my_catalogo', 'component', 1, 1, 1, 10000, 0, '0000-00-00 00:00:00', 0, 0, '', 0, '', 0, 0, 0, '', 1);