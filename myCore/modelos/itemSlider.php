<?php

class ItemSlider extends myEloquent {    
    protected $table = 'my_widget_slider_item';
    protected $fillable = array('id_widget', 'url', 'titulo');
}
