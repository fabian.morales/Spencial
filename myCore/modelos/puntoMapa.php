<?php

class PuntoMapa extends myEloquent {    
    protected $table = 'my_widget_mapa_punto';
    protected $fillable = array('id_widget', 'descripcion', 'latitud', 'longitud', 'info', 'centro');
}
