<?php

class ImagenRef extends myEloquent {    
    protected $table = 'my_img_ref';
    
    public function producto(){
        return $this->belongsTo('Producto', 'id', 'id_referencia');
    }
}
