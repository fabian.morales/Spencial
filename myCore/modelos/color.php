<?php

class Color extends myEloquent {    
    protected $table = 'my_color';
    protected $fillable = array('nombre', 'codigo');
}
