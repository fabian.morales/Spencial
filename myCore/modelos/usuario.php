<?php

class Usuario extends myEloquent {    
    protected $table = 'users';
    protected $fillable = array('id', 'name', 'username', 'email', 'password');
    
    public function compania(){
        return $this->has_one("compania", "id_usuario");
    }
}
