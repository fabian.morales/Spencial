<?php

class CategoriaRef extends myEloquent {    
    protected $table = 'my_categoria';
    protected $fillable = array('nombre', 'id_cat');
    
    function productos(){
        return $this->belongsToMany('Producto', 'my_cat_ref', 'id_categoria', 'id_referencia');
    }
}
