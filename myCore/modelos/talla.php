<?php

class Talla extends myEloquent {    
    protected $table = 'my_talla';
    protected $fillable = array('nombre', 'abrev');
}
